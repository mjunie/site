<?php  
/**
* 
*/
class LanguageLoader 
{
	
	function __construct()
	{
		# code...
	}

	function initialize()
	{
		$CI =& get_instance();
		$CI->load->helper('language');

		$sitelang= $CI->session->userdata('lang');

		if ($sitelang){
			$CI->lang->load('site',$sitelang);

		} else
		$CI->lang->load('site','french');
    }
}