<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site'; //loading default module
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about'] = 'site/about';
$route['contact'] = 'site/contact';
$route['requestdemo'] = 'site/requestademo';


$route['features/student-information-management-system'] = 'site/features/students';
$route['features/staff-management'] = 'site/features/staff';
$route['features/finance-management'] = 'site/features/finances';
$route['features/human-resource-management'] = 'site/features/humanresources';
$route['features/assets-management'] = 'site/features/assets';
$route['features/store-management'] = 'site/features/store';
$route['features/curriculum-management'] = 'site/features/curriculum';


$route['about-story'] = 'site/about/story';
$route['about'] = 'site/about/about';
$route['about-choose'] = 'site/about/choose';
$route['about-mission'] = 'site/about/mission';
$route['about-office'] = 'site/about/office';
$route['about-partners'] = 'site/about/partners';
$route['about-testimonial'] = 'site/about/testimonial';

$route['services/cloud-computing-amazon']   = 'site/services/amazon';
$route['services/cloud-computing-azur']     = 'site/services/azur';
$route['services/cloud-computing-digital']  = 'site/services/digital';
$route['services/cloud-computing-google']   = 'site/services/google';
$route['services/cloud-computing-infrasture']  = 'site/services/infrastructure';
$route['services/corporate-training-helpdesk'] = 'site/services/helpdesk';
$route['services/corporate-training-microsoft'] = 'site/services/microsoft';
$route['services/engineering-development'] = 'site/services/development';
$route['services/engineering-integration'] = 'site/services/integration';
$route['services/engineering-modelling'] = 'site/services/modelling_engineering';
$route['services/erp-business'] = 'site/services/business';
$route['services/erp-crm'] = 'site/services/crm';
$route['services/erp-erp'] = 'site/services/erp';
$route['services/erp-hrm'] = 'site/services/hrm';
$route['services/intelligence-database'] = 'site/services/database';
$route['services/intelligence-intelligence'] = 'site/services/intelligence';
$route['services/intelligence-modelling'] = 'site/services/modelling';
$route['services/security-policies'] = 'site/services/policies';
$route['services/security-risk'] = 'site/services/risk';
$route['services/security-web-application'] = 'site/services/webapp';


$route['industries-banking'] = 'site/industries/banking';
$route['industries-education'] = 'site/industries/education';
$route['industries-health'] = 'site/industries/health';
$route['industries-hospitality'] = 'site/industries/hospitality';
$route['indutries-marketing'] = 'site/industries/marketing';
$route['indusstries-service-businesses'] = 'site/industries/service';
$route['industries-transport'] = 'site/industries/transport';

$route['solutions/business-primeone'] = 'site/solutions/primeone';
$route['solutions/business-primepos'] = 'site/Solutions/primepos';
$route['solutions/business-primesms'] = 'site/Solutions/primesms';
$route['solutions/education-alumnia'] = 'site/Solutions/alumnia';
$route['solutions/education-apson'] = 'site/Solutions/apson';
$route['solutions/education-certverify'] = 'site/Solutions/certverify';
$route['solutions/education-iapply'] = 'site/solutions/iapply';
$route['solutions/education-primecampus'] = 'site/Solutions/primecampus';
$route['solutions/education-revise'] = 'site/solutions/revise';
$route['solutions/events-eventa'] = 'site/solutions/events';
$route['solutions/finance-mifos-x'] = 'site/solutions/mifosx';
$route['solutions/finance-primefinx'] = 'site/Solutions/primefinx';
$route['solutions/finance-primepay'] = 'site/Solutions/primepay';
$route['solutions/general-primecms'] = 'site/Solutions/primecms';
$route['solutions/hospitality-primehotel'] = 'site/Solutions/primehotel';
$route['solutions/social-apson'] = 'site/Solutions/apson_social';
$route['solutions/social-primecx'] = 'site/Solutions/primecx';
$route['solutions/transport-primetlms'] = 'site/Solutions/primetlms';



$route['blog'] = 'blog/blog/index';
$route['blog/post/(:any)'] = 'blog/blog/single_post';

$route['blog/tags/(:any)'] = 'blog/blog/tags';

$route['blog/category/(:any)'] = 'blog/blog/categories';



