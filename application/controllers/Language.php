<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Language extends MY_Controller
{
	
	/*
| -----------------------------------------------------
| PRODUCT NAME: 	Campus360
| -----------------------------------------------------
| AUTHOR:			Nganyu Emmanuel Ndukong
| -----------------------------------------------------
| EMAIL:			emmanuelnganyu@yahoo.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY O'Prime Inc
| -----------------------------------------------------
| WEBSITE:			http://oprime.net
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->helper('language');
	}


	function index($language="french")
	{

		$language = ($language !="") ? $language : "french";

		$this->session->set_userdata('lang',$language);
	
		$this->lang->load('site',$language);

		redirect($_SERVER['HTTP_REFERER']);
	}

	
}