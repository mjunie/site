
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Why Choose Us</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
						<li class="active"><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about">
       						<div class="offices_text choose_text">
								<div class="offices_img">
									<img class="img-fluid" src="<?= site_url()?>/public/site2/img/about/choose1.jpg"  width="840px" height="361px" alt="">
									<div class="offices_img_text">
										<h3>What makes <br />the best for make<br /> work with O'Prime ?</h3>
									</div>
								</div>
								<h3>Why Choose Us as Preority</h3>
								<p>O'Prime is an end-to-end technology provider. We provide full-spectrum technological solutions to our clients. We also boast of our experience and the skills of our team to accompany your company. Above all, we focus on. </p>
							</div>
       						<div class="mission_topic_inner choose_topic">
       							<div class="media">
       								<div class="d-flex">
       									<h3>Cusomer Centricity</h3>
       								</div>
       								<div class="media-body">
       									<p>We pay attention to our clients. We pay attention to their needs. Most importantly we pay attention to their business objectives and drive our solution to fit in perfectly.</p>
       								</div>
       							</div>
       							<!-- <div class="media">
       								<div class="d-flex">
       									<h3>Deep Exercise</h3>
       								</div>
       								<div class="media-body">
       									<p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop.</p>
       								</div>  
       							</div>-->
       							<div class="media">
       								<div class="d-flex">
       									<h3>Actionable Solutions</h3>
       								</div>
       								<div class="media-body">
       									<p>Our solutions combine research, precision, and innovation. We provide perfect fitting solutions for any client problem.</p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h3>Quality Assurance</h3>
       								</div>
       								<div class="media-body">
       									<p>We ensure and assure quality in the services and products we provide to our clients. With guarantee after-sales services, we transform clients into partners.</p>
       								</div>
       							</div>
       						 <!--	<div class="media">
       								<div class="d-flex">
       									<h3>Benifits</h3>
       								</div>
       								<div class="media-body">
       									<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals.</p>
       									<div class="experience_box">
       										<h6>Best Approach Towards Clients</h6>
       										<p>We engage across all levels of clients’ organizations, ensuring buy in and ownership for the solution.</p>
       									</div>
       								</div>
       							</div>  -->
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li ><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li ><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li ><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li class="active"><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
      