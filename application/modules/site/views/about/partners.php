
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Partners</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
						<li class="active"><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about">
       						<div class="awards_main_text partners_text">
								<div class="awards_img">
									<img class="img-fluid" src="<?= site_url()?>/public/site2/img/about/partner.png"  width ="540px" height="361px" alt="">
									<div class="award_img_box">
										<h4>Our partners makes us happy to serve our clients</h4>
									</div>
								</div>
								<div class="award_text">
									<h3 class="small_title">You can checkout our partners list</h3>
									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate <br />one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for<br /> state of the art customer service. Objectively innovate empowered manufactured products whereas parallel plat-forms. Holisticly predominate extensible testing procedures for reliable supply chains. </p>
								</div>
							</div>
       						<div class="partners_items">
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-5.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Visual Edge</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-1.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Fresh Chicks</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-2.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Mon Cheri</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-3.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Strarfield</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-4.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Tuffin</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<img src="<?= site_url()?>/public/site2/img/client/client-5.png" alt="">
       								</div>
       								<div class="media-body">
       									<h4>Soulful</h4>
       									<p>Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. </p>
       								</div>
       							</div>
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li ><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li ><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li class="active"><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
       