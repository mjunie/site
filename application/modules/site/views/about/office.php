        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Our Offices</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="index.html">Advotis</a></li>
						<li><a href="about-us.html">About Us</a></li>
						<li class="active"><a href="offices.html">Our Offices</a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about">
							<div class="offices_text">
								<div class="offices_img">
									<img class="img-fluid" src="<?= site_url()?>/public/site2/img/offices.jpg" alt="">
									<div class="offices_img_text">
										<h3>We are expending around the world</h3>
									</div>
								</div>
								<h3>Our Offices</h3>
								<p>Neighborhood nearness in five urban areas crosswise over America and in Singapore.</p>
								<p>Advotis Consulting has executed commitment internationally serving customers in APAC, Middle East, Europe, Asia and America</p>
							</div>
       						<div class="offices_map_location">
								<div class="row">
									<div class="col-lg-3">
										<h3 class="loc_name">Singapore</h3>
									</div>
									<div class="col-lg-5 col-md-6">
										<address class="location_details">
											<p>Level 30, Six Battery Road, Frist Street Singapore 049909</p>
											<p class="tel">Phone : 1800 456 7890</p>
											<p class="tel">Fax : 1700 897 6654</p>
											<p class="tel">Email : singapur@advotis.com</p>
											<p class="tel">Website : <a class="red" href="#">advotisglobal.com</a></p>
										</address>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="map_box">
											<div id="mapBox2" class="mapBox2 row m0" 
												data-lat="40.701083" 
												data-lon="-74.1522848" 
												data-zoom="11" 
												data-marker="<?= site_url()?>/public/site2/img/map-marker.png"  
												data-info="54B, Tailstoi Town 5238 La city, IA 522364"
												data-mlat="40.701083"
												data-mlon="-74.1522848">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3">
										<h3 class="loc_name">New York</h3>
									</div>
									<div class="col-lg-5 col-md-6">
										<address class="location_details">
											<p>854 Lorance Road, Rose Vallery,<br /> Orlando, New York 8564,<br /> United States.</p>
											<p class="tel">Phone : 1800 456 7890</p>
											<p class="tel">Fax : 1700 897 6654</p>
											<p class="tel">Email : singapur@advotis.com</p>
											<p class="tel">Website : <a class="red" href="#">advotisglobal.com</a></p>
										</address>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="map_box">
											<div id="mapBox3" class="mapBox2 row m0" 
												data-lat="40.701083" 
												data-lon="-74.1522848" 
												data-zoom="11" 
												data-marker="<?= site_url()?>/public/site2/img/map-marker.png"  
												data-info="54B, Tailstoi Town 5238 La city, IA 522364"
												data-mlat="40.701083"
												data-mlon="-74.1522848">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3">
										<h3 class="loc_name">Australia</h3>
									</div>
									<div class="col-lg-5 col-md-6">
										<address class="location_details">
											<p>854 Rose Vallery, Lorance Road, <br />Orlando, Australia 8564,</p>
											<p class="tel">Phone : 1800 456 7890</p>
											<p class="tel">Fax : 1700 897 6654</p>
											<p class="tel">Email : singapur@advotis.com</p>
											<p class="tel">Website : <a class="red" href="#">advotisglobal.com</a></p>
										</address>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="map_box">
											<div id="mapBox4" class="mapBox2 row m0" 
												data-lat="40.701083" 
												data-lon="-74.1522848" 
												data-zoom="11" 
												data-marker="<?= site_url()?>/public/site2/img/map-marker.png"  
												data-info="54B, Tailstoi Town 5238 La city, IA 522364"
												data-mlat="40.701083"
												data-mlon="-74.1522848">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-3">
										<h3 class="loc_name">India</h3>
									</div>
									<div class="col-lg-5 col-md-6">
										<address class="location_details">
											<p>101, Suraj Prakash, First floor, 86<br /> Shankar Ghanekar Marg, <br />Mumbai 2001009</p>
											<p class="tel">Phone : 1800 456 7890</p>
											<p class="tel">Fax : 1700 897 6654</p>
											<p class="tel">Email : singapur@advotis.com</p>
											<p class="tel">Website : <a class="red" href="#">advotisglobal.com</a></p>
										</address>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="map_box">
											<div id="mapBox5" class="mapBox2 row m0" 
												data-lat="40.701083" 
												data-lon="-74.1522848" 
												data-zoom="11" 
												data-marker="<?= site_url()?>/public/site2/img/map-marker.png"  
												data-info="54B, Tailstoi Town 5238 La city, IA 522364"
												data-mlat="40.701083"
												data-mlon="-74.1522848">
											</div>
										</div>
									</div>
								</div>
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li ><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li class="active"><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
   