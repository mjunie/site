
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Mission & Values</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
						<li class="active"><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about"> 
							<div class="mission_main_text">
								<img class="img-fluid" src="<?= site_url()?>/public/site2/img/about/mission.jpg" width="690px" height="296.53px" alt="">
								<h3 class="small_title"><?=lang('')?>Our Mission & Values makes us best</h3>
								<p> <?=lang('Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service. Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains.')?> </p>
							</div>
       						<div class="mission_topic_inner">
       							<div class="media">
       								<div class="d-flex">
       									<h3><?=lang('Mission')?></h3>
       								</div>
       								<div class="media-body">
       									<p><?=lang('Mission: To accelerate growth across the African continent through digital transformation of small and medium size businesses. ')?></p>
       									<h4><?=lang('To achieve this mission, we are committed to;')?> </h4>
       									<ul class="nav flex-column">
       										<li><a href="#"><?=lang('Develop Cost effective alternative solutions for SMB’s')?></a></li>
       										<li><a href="#"><?=lang(' Accompany SMB’s through a full digital transformation process')?></a></li>
       										<li><a href="#"><?=lang('Equip our team with the knowledge and tools to successfully accompany our clients through digital transformation')?></a></li>
       										<li><a href="#"><?=lang('Always satisfy our clients.')?></a></li>
       										
       										
       									</ul>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h3><?=lang('Our Values')?></h3>
       								</div>
       								<div class="media-body">
       									<p><?=lang('An Africa with businesses thriving at their full potentials due to proper application of technology. To achieve this vision, we are committed to;')?></p>
       									<ul class="nav flex-column">
       										<li><a href="#"><?=lang('Provide our clients with the tools and solutions for effective digital transformation')?> </a></li>
       										<li><a href="#"><?=lang('Guide our clients with technology decisions that will impact their growth')?> </a></li>
       										
       									</ul>
       									<div class="experience_box">
       										<h6><?=lang('')?>25 Year’s Of Experience</h6>
       										<p><?=lang('')?>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration </p>
       									</div>
       								</div>
       							</div>
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li ><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li class="active"><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
       