
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>About Us</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="index.html">O'Prime</a></li>
						<li class="active"><a href="#">About Us</a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================Investor Area =================-->
        <section class="investor_area p_100">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-6">
        				<div class="investor_left_text">
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/investor-1.jpg" width="270px" height="210px" alt="">
        						</div>
        						<div class="media-body">
        							<h6>What We Serve</h6>
        							<p>Bring to the table win-win surviv-al strategies to ensure proactive domination. At the end of the day, going forward.</p>
        						</div>
        					</div>
        					<h3>Reviving Industries Through Technology Solutions</h3>
        					<p>
                                                Innovation is a way of life at O'Prime. Our innovative solutions completely transform whole industries, reviving companies, and launching them into the digital age.
                                                </p>
        					<p>
                                                  With a young research and development team accompanied by reliable industry partners, we have identified industry shortcomings and developed technology solutions that match the exact needs of budget, usability, and efficiency.
      
                                                </p>
        				</div>
        			</div>
        			<div class="col-lg-6">
        				<div class="investor_right_text">
        					<h3>An Industry leader in technology services.</h3>
        					<p>O’Prime is a technology consulting firm founded on the need to provide key technical support and infrastructure to businesses. 
                                                O'Prime focuses on developing and providing the necessary I.T solutions that companies will need to sustain their business processes and better satisfy clients.</p>
        					<p>Through established partnerships and accreditations, O'Prime is a an industry leader in technology innovation.</p>
        					<a class="main_btn" href="#">Download our brochure</a>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Investor Area =================-->
        
        <!--================Effective Area =================-->
        <section class="effective_area effective_two">
        	<div class="effect_inner">
				<div class="left_text">
					<div class="effect_text_inner">
						<div class="effect_text">
        							<h3>5 Years of Effective Business Management & Consulting Solutions</h3>
                						 <p>Our growth in the last five years is evidence of our commitment to innovation. 
                                                                         Continually breaking boundaries and opening up new opportunities for ourselves and our partners has been our key to survival.
                                                                         We are proud of our achievements this far and only hope for a better tomorrow.
                                                                 </p>
                                                         </div>
							<div class="row effect_item_inner">
								<div class="col-sm-6">
									<div class="effect_item">
										<i  class="flaticon-support"></i>
										<h4> Customer Satisfaction</h4>
									 <p> 
                                                                                Yes! Customer is also king to us, prioritizing customer projects and yielding satisfaction is a core principle.

                                                                           </p>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="effect_item">
										<i class="flaticon-idea"></i>
										<h4>Strategy & Planning</h4>
									    <p>Bring to the table win-win survival strategies to ensure proactive domination. 
                                                                                                                                                          </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="right_img">
					<img src="<?= site_url()?>/public/site2/img/about1.jpg" alt="">
				</div>
        	</div>
        </section>
        <!--================End Effective Area =================-->
        <div class="border_class"><div class="container"></div></div>
        <!--================Client Slider Area =================-->
        <section class="tt_client_area">
        	<div class="container">
        		<div class="center_title">
        			<h2>Trusted Clients</h2>
        			<p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster</p>
        		</div>
        		<div class="client_slider owl-carousel">
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-1.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-2.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-3.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-4.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-5.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-1.png" alt="">
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Client Slider Area =================-->
        
        <!--================Conversion Area =================-->
        <section class="conversion_area">
        	<div class="container">
        		<div class="conversion_inner">
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="conversion_text">
        						<h3>Want to continue the conversion?</h3>
        						<h4>Speak to our expert in</h4>
								<select>
									<option value="1">Select an industry</option>
									<option value="2">Another option</option>
									<option value="3">Potato</option>
								</select>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="conversion_subs">
        						<p>We help global leaders with their organization's most critical issues and opportunities. Together, we create enduring change and results.</p>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn submit_btn" type="submit" id="button-addon2">contact us</button>
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Conversion Area =================-->
       
    