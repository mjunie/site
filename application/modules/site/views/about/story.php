
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Our Story</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
						<li class="active"><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about">
							<div class="story_main_text">
								<div class="media">
									<div class="d-flex">
										<img class="img-fluid" src="<?= site_url()?>/public/site2/img/about/story1.jpg"  width="389px"  height="361px" alt="">
									</div>
									<div class="media-body">
										<h3>Our Story</h3>
										<p>O’Prime was created in 2015 in Douala, Cameroon. Initially Registered as I.T Solutions Cameroon, the name will later be changed to O’Prime in 2020 following a complete restructuring of the company. 
                     </p>
									</div>
								</div>
								<p>Over the past five years, the company has served a diverse set of clients and matured to a leader in the industry. Today we boast of new innovative products and services, partnerships, awards including accreditations nationally and internationally.
                </p>
							</div>
       						<div class="story_time_line">
       							<div class="media">
       								<div class="d-flex">
       									<h5>2015</h5>
       								</div>
       								<div class="media-body">
       									<h4>Company Started</h4>
       									<p>Registered as I.T Solutions, providing I.T Consulting and software development. Develops DocuPerdu. 
                        </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h5>2016</h5>
       								</div>
       								<div class="media-body">
                       <h4>Improved Company</h4>
       									<p>Develops Univ-Portal and Ndolo360.  Continues  exploitation of DocuPerdu.</p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h6>25 Dec.</h6>
       									<h5>2017</h5>
       								</div>
       								<div class="media-body">
       									<h4>Award Win</h4>
       									<p>Wins 2nd Place at Nextell Startup Award with Service DocuPerdu. Partners with 25 universities for Univ-Portal. Increases team and client pool. Recognized by the Africa Entrepreneurship Award team as a “100 Most Innovative startups in Africa”
                       </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h5>2018</h5>
       								</div>
       								<div class="media-body">
       									<h4>Open 4 New Branches</h4>
       									<p>Launch PrimeSMS, Events.cm and University.cm. Launches the first magazine on higher education in    Cameroon, UNIVERSITY.
                          </p>
       								</div>
       							</div>
       							<div class="media">
       								<div class="d-flex">
       									<h6>1 Jan.</h6>
       									<h5>2019</h5>
       								</div>
       								<div class="media-body">
                        <h4>Expand Internaltionally</h4>
       									<p>Launches PrimeOne – Business Management suite, AKADEMIA – Social network and Revise, Mobile revision app. Partners with MIFOS X.</p>
       								</div>
       							</div>
                    <div class="media">
                      <div class="d-flex">
                        <h5>2020 </h5>
                      </div>
                      <div class="media-body">
                        <h4>Expand Internaltionally</h4>
                        <p>Launches – PrimeCampus- Academic Management ERP, PrimeHotels – Hotel Management ERP, PrimeCMS – Multilingual Website Management System, PrimeFINX, APSON and much more…
                          </p>
                      </div>
                    </div>
       						</div>
       						<div class="story_values">
       							<div class="media">
       								<div class="d-flex">
       									<h3>Our Values</h3>
       								</div>
       								<div class="media-body">
       									<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. </p>
       									<div class="row story_box_inner">
       										<div class="col-md-6">
       											<div class="story_box">
       												<h5>Work Strategy</h5>
       												<p>Proactively envisioned multimedia based expertise and cross media growth strategies. </p>
       											</div>
       										</div>
       										<div class="col-md-6">
       											<div class="story_box">
       												<h5>Work Strategy</h5>
       												<p>Proactively envisioned multimedia based expertise and cross media growth strategies. </p>
       											</div>
       										</div>
       									</div>
       								</div>
       							</div>
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li class="active"><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
       