
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2>Testimonials</h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
						<li class="active"><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="main_area_about">
       						<div class="awards_main_text testi_text">
								<div class="awards_img">
									<img class="img-fluid" src="<?= site_url()?>/public/site2/img/about/testimonial1.jpg" alt="">
									<div class="award_img_box">
										<h4><i>“</i><br />What our <br /> clients Says about us</h4>
									</div>
								</div>
								<div class="award_text">
									<h3 class="small_title">Testimonials</h3>
									<p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</p>
								</div>
							</div>
       						<div class="testi_list">
       							<div class="testi_item">
       								<div class="media">
       									<div class="d-flex">
       										<span>“</span>
       									</div>
       									<div class="media-body">
       										<p> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
											<div class="media">
												<div class="d-flex">
													<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/testimonials/testi-1.png" alt="">
												</div>
												<div class="media-body">
													<h4>Marry Devolis</h4>
													<h6>Minda Director</h6>
												</div>
											</div>
       									</div>
       								</div>
								</div>
       							<div class="testi_item">
       								<div class="media">
       									<div class="d-flex">
       										<span>“</span>
       									</div>
       									<div class="media-body">
       										<p> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
											<div class="media">
												<div class="media-body">
													<h4>Marry Devolis</h4>
													<h6>Minda Director</h6>
												</div>
											</div>
       									</div>
       								</div>
								</div>
      							<div class="testi_item">
       								<div class="media">
       									<div class="d-flex">
       										<span>“</span>
       									</div>
       									<div class="media-body">
       										<p> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
											<div class="media">
												<div class="d-flex">
													<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/testimonials/testi-2.png" alt="">
												</div>
												<div class="media-body">
													<h4>Marry Devolis</h4>
													<h6>Minda Director</h6>
												</div>
											</div>
       									</div>
       								</div>
								</div>
      							<div class="testi_item">
       								<div class="media">
       									<div class="d-flex">
       										<span>“</span>
       									</div>
       									<div class="media-body">
       										<p> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
											<div class="media">
												<div class="media-body">
													<h4>Marry Devolis</h4>
													<h6>Minda Director</h6>
												</div>
											</div>
       									</div>
       								</div>
								</div>
       						</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li ><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
								<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
								<li ><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
								<li ><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
								<li ><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
								<li class="active"><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
        <!--================Conversion Area =================-->
        <section class="conversion_area p-0">
        	<div class="container">
        		<div class="conversion_inner">
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="conversion_text">
        						<h3>Want to continue the conversion?</h3>
        						<h4>Speak to our expert in</h4>
								<select>
									<option value="1">Select an industry</option>
									<option value="2">Another option</option>
									<option value="3">Potato</option>
								</select>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="conversion_subs">
        						<p>We help global leaders with their organization's most critical issues and opportunities. Together, we create enduring change and results.</p>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn submit_btn" type="submit" id="button-addon2">contact us</button>
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Conversion Area =================-->
       
    