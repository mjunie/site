
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2><?= lang('Events')?></h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="#">Solutions</a></li>
						<li class="active"><a href="<?= site_url('solutions/events-eventa')?>"><?= lang('Events')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="automobiles_inner pad_left_30">
        					<div class="automobile_sector">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/financial.jpg" alt="">
        						<h3>Banking & Financial Sectors</h3>
        						<div class="row">
        							<div class="col-lg-6">
        								<div class="left">
        									<p>Proactively envisioned multimedia based expertise and cross-media growth strategies.</p>
        									<p>Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.  Phosfluorescently engage worldwide methodologies with web.</p>
        								</div>
        							</div>
        							<div class="col-lg-6">
        								<div class="right">
        									<p>Proactively envisioned multimedia based exper-tise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.  </p>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="mobile_experience color_full">
        						<div class="main_title">
        							<h2>Sub Segments we have Experience in</h2>
        							<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior</p>
        						</div>
        						<div class="row">
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item">
        									<h4>Auto Engineering</h4>
        									<p>Proactively envisioned multimedia based exper-tise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        								</div>
        							</div>
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item red">
        									<h4>Auto Mechnical</h4>
        									<p>Proactively envisioned multimedia based exper-tise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        								</div>
        							</div>
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item green">
        									<h4>Advanced Auto</h4>
        									<p>Proactively envisioned multimedia based exper-tise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="groth_brains">
        						<div class="row">
        							<div class="col-sm-8">
										<div class="groth_items">
											<div class="media">
												<div class="d-flex">
													<img src="<?= site_url()?>/public/site2/img/icon/groth-1.png" alt="">
												</div>
												<div class="media-body">
													<h4>Growth of Model</h4>
													<p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables.</p>
												</div>
											</div>
											<div class="media">
												<div class="d-flex">
													<img src="<?= site_url()?>/public/site2/img/icon/groth-2.png" alt="">
												</div>
												<div class="media-body">
													<h4>Advotis Brains</h4>
													<p>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables.</p>
												</div>
											</div>
										</div>
        							</div>
        							<div class="col-sm-4">
        								<div class="groth_img">
        									<img class="img-fluid" src="<?= site_url()?>/public/site2/img/groth-2.jpg" alt="">
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="tab_typical">
        						<h3 class="single_title">Typical Assignments Executed By Us</h3>
        						<ul class="nav nav-tabs" id="myTab" role="tablist">
        							<li class="nav-item">
        								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Strategy</a>
        							</li>
        							<li class="nav-item">
        								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Performance Improvement</a>
        							</li>
        							<li class="nav-item">
        								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Transactions</a>
        							</li>
        						</ul>
        						<div class="tab-content" id="myTabContent">
        							<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        								<ul class="nav flex-column">
        									<li><a href="#">Completely synergize resource taxing relationships via premier niche markets. </a></li>
        									<li><a href="#">Vate one-to-one customer service with robust ideas</a></li>
        									<li><a href="#">Dynamically innovate resource-leveling customer service for state</a></li>
        									<li><a href="#">Objectively innovate empowered manufactured products whereas parallel platforms</a></li>
        								</ul>
        							</div>
        							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        								<ul class="nav flex-column">
        									<li><a href="#">Completely synergize resource taxing relationships via premier niche markets. </a></li>
        									<li><a href="#">Vate one-to-one customer service with robust ideas</a></li>
        									<li><a href="#">Dynamically innovate resource-leveling customer service for state</a></li>
        									<li><a href="#">Objectively innovate empowered manufactured products whereas parallel platforms</a></li>
        								</ul>
        							</div>
        							<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        								<ul class="nav flex-column">
        									<li><a href="#">Completely synergize resource taxing relationships via premier niche markets. </a></li>
        									<li><a href="#">Vate one-to-one customer service with robust ideas</a></li>
        									<li><a href="#">Dynamically innovate resource-leveling customer service for state</a></li>
        									<li><a href="#">Objectively innovate empowered manufactured products whereas parallel platforms</a></li>
        								</ul>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">
								<li class="active"><a href="<?= site_url('solutions/events-eventa')?>"><?= lang('Events')?></a></li>
							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
   