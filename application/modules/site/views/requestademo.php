<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?=lang('Request for a demo') ?></h1>
                    <p><?=lang('Hi, Fill in the form below to get a demo setup for you.') ?><span></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact Info**
=================================================== -->
        <section class="contact-outer padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-10">
                        <ul class="row contact-list">
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-contact"></span></div>
                                    <h3><?=lang('Contact Info') ?></h3>
                                    <p><?=lang('Your Name') ?></p>
                                    <div class="call"><span class="icon-phone"></span> (237) 693-527-975</div>
                                    <a href="mailto:info@campus360.com" class="mail-to"><span class="icon-mail"></span> info@campus360.com</a> </div>
                            </li>
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-live-chat"></span></div>
                                    <h3>Live Chat</h3>
                                    <p><?=lang('Chat with a consultant directly') ?></p>
                                    <a href="#" class="live-chat"><span class="icon-chat-bubble"></span>WhatsApp Chat</a> </div>
                            </li>
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-support-ticket"></span></div>
                                    <h3><?=lang('Free Training') ?></h3>
                                    <p><?=lang('Train your team on c360 for free') ?></p>
                                    <a href="#" class="live-chat support">WhatsApp Chat</a> </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Start Support Request -->
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <div class="support-request">
                            <h2><?=lang('Demo Request') ?></h2>
                            <p><?=lang('Fill the form below with your details and a demo will be setup for you. A consultant will also be assigned to work with you to setup the demo and follow up with you throughout the test phase.') ?></p>
                            
                            <form action="<?=site_url('site/senddemorequest');?>" method="post" class="support-form" >
                                <?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-danger " role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                                <p><?php echo $this->session->flashdata('error');?></p>
                                            </div>
                                              <?php } else if($this->session->flashdata('success')){?>

                                             <div class="alert alert-info " role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                                <p><?php echo $this->session->flashdata('success');?></p>
                                            </div>
                                              <?php }?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input name="name" type="text"  placeholder="<?=lang('Your Name')?>">
                                        <input name="email" type="text" placeholder="Email">
                                        <input name="phone" type="text" placeholder="<?=lang('Telephone')?>">
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input name="company" type="text" placeholder="Institution">
                                        <textarea name="message" placeholder="<?=lang('Tell us more about your institution, your challenges and how you hope Campus360 will help you') ?>" rows="40"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="submit-btn"><?=lang('Send') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Advices and Answers**
=================================================== -->
        <!-- <section class="advices-outer padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <h2>Advice and answers from Our Team</h2>
                        <form action="#" method="get" class="search-outer d-flex">
                            <input name="Search" type="text" placeholder="Search">
                            <button class="go-btn"><span class="icon-search"></span></button>
                        </form>
                    </div>
                </div>
                <ul class="row features-listing">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/info-ico.png" alt=""></span>
                            <h3>Information about <span>Protech portal</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/payment-ico.png" alt=""></span>
                            <h3>Protech portal <span>Payment</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/paying-ico.png" alt=""></span>
                            <h3>Paying with <span>Protech portal</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/wallet-ico.png" alt=""></span>
                            <h3>Secure Your <span>Payment Method</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/building-ico.png" alt=""></span>
                            <h3>Businesses & <span>Organizations</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/doc-ico.png" alt=""></span>
                            <h3>Protech portal <span>Documentation</span></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since been</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
 -->