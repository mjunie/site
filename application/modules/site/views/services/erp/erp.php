
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2><?= lang('ERP Consulting ')?></h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="#">Services</a></li>
						<li class="active"><a href="<?= site_url('services/erp-erp')?>"><?= lang('ERP Consulting ')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================Client Image Area =================-->
        <section class="client_img_area business_process pad_top">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3">
        				<div class="client_menu">
        					<ul class="nav flex-column">
        				
								<li ><a href="<?= site_url('services/erp-business')?>"><?= lang('Business Process Automation')?></a></li>
								<li class="active"><a href="<?= site_url('services/erp-erp')?>"><?= lang('ERP Consulting ')?></a></li>
								<li ><a href="<?= site_url('services/erp-crm')?>"><?= lang('CRM Consulting')?></a></li>
								<li><a href="<?= site_url('services/erp-hrm')?>"><?= lang('HRM Consulting')?></a></li>
        					</ul>
        				</div>
        			</div>
        			<div class="col-lg-9">
        				<div class="client_img">
        					<img class="img-fluid" src="<?= site_url()?>/public/site2/img/strategy.jpg" alt="">
        				</div>
        			</div>
        		</div>
        		
        	</div>
        </section>
        <!--================End Client Image Area =================-->
        
        <!--================Performance Area =================-->
        <section class="performance_area">
        	<div class="container">
        		<div class="process_action">
        			<h4>Our strategy practice helps management devise winning strategies and translate them into action</h4>
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="left">
        						<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. </p>
        						<p>Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.  Phosfluorescently engage worldwide methodologies with web.</p>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="right">
        						<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products. </p>
        						<a class="more_btn" href="#">Know more about us</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Performance Area =================-->
        
        <!--================Facts Area =================-->
        <section class="facts_area strategy_fact p_100">
        	<div class="container">
        		<div class="row facts_inner">
        			<div class="col-lg-3 col-sm-6">
        				<div class="facts_title">
        					<h2>Some Facts About Us</h2>
        					<p>Proactively envisioned multime-dia based expertise and cross media growth strategies. </p>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="fact_item basecolor">
        					<h4>Featured Strategy</h4>
        					<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="fact_item red">
        					<h4>Segment Strategy</h4>
        					<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="fact_item green">
        					<h4>Strategy info</h4>
        					<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seam lessly visualize quality intellectual capital.</p>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Facts Area =================-->
        
        <!--================Business Process Area =================-->
        <div class="business_process_area p_100">
        	<div class="container">
        		<div class="process_text text-center">
        			<h3>We help boost corporate performance by supporting management with the tools they need to deliver.</h3>
        			<p>Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellec-tual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.  Phosfluorescently engage worldwide methodologies with web. Multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. </p>
        			<a class="main_btn" href="#">let’s work together</a>
        		</div>
        		<div class="row process_items">
        			<div class="col-lg-4 col-sm-6">
        				<div class="company_item">
        					<div class="company_text">
        						<a href="#"><h4>Group portfolio and diversfication strategy</h4></a>
        						<p>Proactively envisioned multimedia based expertise and cross-media growth strate-gies. Seamlessly visualize quality intel-lectual capital without superior.</p>
        						<a class="more_btn" href="#">Read more</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="company_item">
        					<div class="company_text">
        						<a href="#"><h4>Shared Service <br />Strategy</h4></a>
        						<p>Proactively envisioned multimedia based expertise and cross-media growth strate-gies. Seamlessly visualize quality intel-lectual capital without superior.</p>
        						<a class="more_btn" href="#">Read more</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="company_item">
        					<div class="company_text">
        						<a href="#"><h4>Talent & Synergies Strategy</h4></a>
        						<p>Proactively envisioned multimedia based expertise and cross-media growth strate-gies. Seamlessly visualize quality intel-lectual capital without superior.</p>
        						<a class="more_btn" href="#">Read more</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
        <!--================End Business Process Area =================-->
        
        <!--================Conversion Area =================-->
        <section class="conversion_area pad_top">
        	<div class="container">
        		<div class="center_title">
        			<h2>Management devise winning strategies</h2>
        			<h5>How you can get your goal</h5>
        			<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procra-stinate B2C users after installed base benefits. Dramatically visualize customer directed conver-gence without revolutionary ROI.</p>
        		</div>
        		<div class="conversion_inner">
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="conversion_text">
        						<h3>Want to continue the conversion?</h3>
        						<h4>Speak to our expert in</h4>
								<select>
									<option value="1">Select an industry</option>
									<option value="2">Another option</option>
									<option value="3">Potato</option>
								</select>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="conversion_subs">
        						<p>We help global leaders with their organization's most critical issues and opportunities. Together, we create enduring change and results.</p>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn submit_btn" type="submit" id="button-addon2">contact us</button>
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Conversion Area =================-->
       
      