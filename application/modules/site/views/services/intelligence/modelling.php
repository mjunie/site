
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2><?= lang('Data Modelling')?></h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="#">Services</a></li>
						<li class="active"><a href="<?= site_url('services/intelligence-modelling')?>"><?= lang('Data Modelling')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================About Us Story Area =================-->
        <section class="about_story_area p_100">
        	<div class="container">
        		<div class="row flex-row-reverse">
        			<div class="col-lg-9">
        				<div class="automobiles_inner pad_left_30">
        					<div class="automobile_sector">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/financial.jpg" alt="">
        						<h3><?= lang('Data Modelling')?></h3>
        						<div class="row">
        							<div class="col-lg-6">
        								<div class="left">
        									<p> <?= lang('Data and Process Modeling is a way of developing a graphical model that shows how a system converts data into valuable information. The result of such modeling is a logical model that provides support for business operations and ensures that user is needs are fulfilled.')?></p>
        								</div>
        							</div>
        							<div class="col-lg-6">
        								<div class="right">
        									<p><?= lang('O’Prime comes with an experienced team in data modelling.  Working with your team, we will outline your business cases to develop a data model that will work best for you.  The deliverables could be a conceptual data model, a logical data model or a physical data model.')?> </p>
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="mobile_experience color_full">
        						<div class="main_title">
        							<h2><?= lang('Business Intelligence')?></h2>
        							<p><?= lang('Business intelligence (BI) is a technology-driven process for analyzing data and presenting actionable information which helps executives, managers and other corporate end users make informed business decisions.')?>
                                                                </p>
                                                                <p><?= lang('For years many companies have been collecting data from clients, sales, finance, marketing, etc, and often focus more on transactional processes rather than analytical processes. ')?>
                                                                                                                                        </p>
        						</div>
        						<div class="row">
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item">
        									<h4><?= lang('')?>Auto Engineering</h4>
        									<p><?= lang('Business intelligence opens the possibility to harmonize data from various business units, analyze them through various matrices, and provide results that could be used to influence decision making in companies.')?></p>
        								</div>
        							</div>
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item red">
        									<h4><?= lang('')?>Auto Mechnical</h4>
        									<p><?= lang('Insightful decisions are most likely to positively influence a business. Generating insights through business data is a combination of tools, techniques, and experience.')?>

                                                                                </p>
        								</div>
        							</div>
        							<div class="col-lg-4 col-sm-6">
        								<div class="ex_item green">
        									<h4><?= lang('')?>Advanced Auto</h4>
        									<p><?= lang('O Prime provides business intelligence consulting In varying business sectors such as;
                                                                                Banking & Finance
                                                                                Wholesale, Retail & Distribution
                                                                                Telecommunications
                                                                                Marketing
                                                                                Transports & Logistics
                                                                                Our tools of preference are
                                                                                Looker – BY Google
                                                                                PowerBI – By Microsoft
                                                                                Tableau')?>
                                                                                </p>
        								</div>
        							</div>
        						</div>
        					</div>
        					
        					<div class="tab_typical">
        						<h3 class="single_title"><?= lang('Development and Automation')?></h3>
                                                        <p><?= lang('In software engineering, development begins after / or alongside systems modelling. 
                                                                Systems development (Coding or programming) involves converting the systems models into aactual programs through a programming language. 
                                                                The choice of programming languages used depends on several factors such as;')?>
                                                                

                                                        </p>
        						<ul class="nav nav-tabs" id="myTab" role="tablist">
        							<li class="nav-item">
        								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?= lang('The type of system')?></a>
        							</li>
        							<li class="nav-item">
        								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><?= lang('The cost of the system')?></a>
        							</li>
        							<li class="nav-item">
        								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><?= lang('The Size of the system')?></a>
        							</li>
        						</ul>
        						<div class="tab-content" id="myTabContent">
        							<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        								<ul class="nav flex-column">
        									<li><?= lang('The type of system: Some programming languages are most suitable particular projects than others. C and Java are most suitable for mission critical systems e.g banking and embedded systems, PHP is most suitable for Web Applications and ERP. Python is most suitable for AI projects because of her rich libraries and so on.')?></li>
        									
        								</ul>
        							</div>
        							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        								<ul class="nav flex-column">
        									<li><?= lang('The cost of the system: Working with some programming languages tend to be more expensive than others especially due to the lack of developers in that language. PHP projects are the cheapest because of an abundance of developers while C, C#, Java and Python could be more expensive. Other costs could be attributed to server costs and licenses.')?>
                                                                                </li>
        									
        								</ul>
        							</div>
        							<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        								<ul class="nav flex-column">
        									<li><?= lang('The Size of the system: Large and complex systems often require a combination of multiple programming languages and / or frameworks. The choice of technology stack varies from one team to another.')?> </li>
        									
        								</ul>
        							</div>
        						</div>
        					</div>
                                                <div class="groth_brains">
                                                        <div class="row">
                                                                <div class="col-sm-8">
                                                                                <div class="groth_items">
                                                                                        <div class="media">
                                                                                                <div class="d-flex">
                                                                                                        <img src="<?= site_url()?>/public/site2/img/icon/groth-1.png" alt="">
                                                                                                </div>
                                                                                                <div class="media-body">
                                                                                                        <h4><?= lang('')?>Growth of Model</h4>
                                                                                                        <p><?= lang('Engineers at O’Prime have experiences in various programming languages and technology stacks. Skills are regularly upgraded to meet new standards set by emerging platforms.')?> </p>
                                                                                                </div>
                                                                                        </div>
                                                                                        <div class="media">
                                                                                                <div class="d-flex">
                                                                                                        <img src="<?= site_url()?>/public/site2/img/icon/groth-2.png" alt="">
                                                                                                </div>
                                                                                                <div class="media-body">
                                                                                                        <h4><?= lang('')?>Advotis Brains</h4>
                                                                                                        <p><?= lang('Automation of development and production environment is key in .the lifecycle of various systems. System automation may include load balancing, file transfers, database backup, server upgrades, security checks etc. 
                                                                                                        Our team can use various technologies to automate deployment and management of life systems. ')?>
                                                                                                        </p>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                        <div class="groth_img">
                                                                                <img class="img-fluid" src="<?= site_url()?>/public/site2/img/groth-2.jpg" alt="">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<aside class="menu_widget">
							<ul class="nav flex-column">

								<li ><a href="<?= site_url('services/intelligence-database')?>"><?= lang('Database Administration')?> </a></li>
								<li class="active"><a href="<?= site_url('services/intelligence-modelling')?>"><?= lang('Data Modelling')?></a></li>
								<li ><a href="<?= site_url('services/intelligence-intelligence')?>"><?= lang('Business Intelligence')?></a></li>

							</ul>
						</aside>
        				<div class="story_left_sidebar">
        					<aside class="left_widget insight_widget">
        						<div class="f_title">
									<h3>Insights</h3>
        							<span></span>
        						</div>
        						<div class="insight_inner">
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        							<div class="insight_item">
        								<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
        								<a href="#">October 25, 2018</a>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget says_widget">
        						<div class="f_title">
									<h3>CEO Says</h3>
        							<span></span>
        						</div>
        						<div class="says_inner">
        							<p>Efficiently unleash cross-me-dia information without cross-media value. Quickly maximize timely deliver.</p>
        							<div class="media">
        								<div class="d-flex">
        									<img class="rounded-circle" src="<?= site_url()?>/public/site2/img/ceo-2.png" alt="">
        								</div>
        								<div class="media-body">
        									<h5>John MIchale</h5>
        									<h6>CEO of Advotis</h6>
        								</div>
        							</div>
        						</div>
        					</aside>
        					<aside class="left_widget button_widget">
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
        						<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End About Us Story Area =================-->
       
   