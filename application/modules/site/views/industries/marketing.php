
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area">
        	<div class="breadcrumb_top">
        		<div class="container">
					<h2><?= lang('Marketing Automation')?></h2>
				</div>
        	</div>
        	<div class="breadcrumb_bottom">
        		<div class="container">
					<ul class="nav">
						<li><a href="#">O'Prime</a></li>
						<li><a href="<?= site_url('indutries-marketing')?>"><?= lang('Marketing Automation')?></a></li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Breadcrumb Area =================-->
        
        <!--================Case Studies Two Area =================-->
        <section class="case_studies_two studies_title p_100">
        	<div class="container">
        		<div class="main_title">
        			<p>We introduces the accompanying contextual investigations that epitomize the sort of<br /> arrangements and administrations we are giving over our <a href="#">client base.</a></p>
        		</div>
        		<div class="studies_two_inner row">
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-1.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Business Increased by 25% Annually</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Besiness Growth</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-2.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Insurance Advice for<br /> logn life</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Insurance Sector</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-3.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Health And harmaceutical</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Health care and life science</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-4.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Business Increased by 25% Annually</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Besiness Growth</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-5.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Insurance Advice for<br /> logn life</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Insurance Sector</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4 col-sm-6">
        				<div class="studies_item bg">
        					<div class="studies_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-two-6.jpg" alt="">
        						<a class="reply" href="#"><img src="<?= site_url()?>/public/site2/img/icon/icon-reply.png" alt=""></a>
        						<a class="img_tt" href="#"><h3>Health And harmaceutical</h3></a>
        					</div>
        					<div class="studies_content">
        						<a href="#"><h4>Health care and life science</h4></a>
        						<p>Objectively innovate empowered manufac-tured products whereas parallel platforms. Holisticly predominate extensible.</p>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Case Studies Two Area =================-->
        
        <!--================Conversion Area =================-->
        <section class="conversion_area p-0">
        	<div class="container">
        		<div class="conversion_inner">
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="conversion_text">
        						<h3>Want to continue the conversion?</h3>
        						<h4>Speak to our expert in</h4>
								<select>
									<option value="1">Select an industry</option>
									<option value="2">Another option</option>
									<option value="3">Potato</option>
								</select>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="conversion_subs">
        						<p>We help global leaders with their organization's most critical issues and opportunities. Together, we create enduring change and results.</p>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn submit_btn" type="submit" id="button-addon2">contact us</button>
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Conversion Area =================-->
       
    