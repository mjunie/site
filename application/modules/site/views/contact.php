<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner contact-banner">
            <div class="container">
                <div class="contents">
                    <h1><?=lang('Get in touch') ?></h1>
                    <p><?=lang('Contact us directly by mail or call through one of the numbers and emails below or drop us a message') ?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact**
=================================================== -->
        <section class="contact-wrapper-outer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 form-area">
                        <div class="contact-form-wrapper padding-lg">

                              <?php if($this->session->flashdata('error')){?>
                        <div class="alert alert-danger " role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                                <p><?php echo $this->session->flashdata('error');?></p>
                                            </div>
                                              <?php } else if($this->session->flashdata('success')){?>

                                             <div class="alert alert-info " role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                                <p><?php echo $this->session->flashdata('success');?></p>
                                            </div>
                                              <?php }?>
                            <form name="contact-form" id="ContactForm" method="post" action="<?=site_url('site/sendcontact');?>">
                                <div class="row">
                                    <div class="col-md-6 input-col">
                                        <label><?=lang('Your Name') ?></label>
                                        <input name="name" placeholder="" type="text">
                                    </div>
                                    <div class="col-md-6 input-col">
                                        <label><?=lang('Email') ?></label>
                                        <input name="email" placeholder="" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 input-col">
                                        <label><?=lang('Phone') ?></label>
                                        <input name="phone" placeholder="" type="text">
                                    </div>
                                    <div class="col-md-6 input-col">
                                        <label><?=lang('Institution') ?></label>
                                        <input name="company" placeholder="" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Message</label>
                                        <textarea name="message" placeholder=""></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn submit"><?=lang('Send') ?></button>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="msg"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-info-wrapper padding-lg">
                            <div class="contact-info">
                                <h3>Contact Info</h3>
                                <ul class="info-contact-box">
                                    <li>
                                        <h6>Campus360</h6>
                                        <p>Douala, Bessengue, Rue 976, Besides Immeuble Polypharma</p>
                                    </li>
                                    <li>
                                        <h6>(237) 693 527 975</h6>
                                    </li>
                                    <li> <a href="#">info@campu360.com</a> </li>
                                </ul>
                            </div>
                            <div class="social-media-box">
                                <h6><span>Connect with</span></h6>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact Map**
=================================================== -->
        <section class="contact-map">
            <div class="msg-box"><p><i class="fa fa-mouse-pointer" aria-hidden="true"></i> click and scroll to zoom the map</p></div>
            <iframe src="https://snazzymaps.com/embed/233849" width="100%" height="600px" style="border:none;"></iframe>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->