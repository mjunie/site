
        
        <!--================Slider Area =================-->
        <section class="main_slider_area">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>
                    <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="<?= site_url()?>/public/site2/img/home-slider/slider-1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?= site_url()?>/public/site2/img/home-slider/slider-1.png"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            <div class="tp-caption tp-resizeme digit_text"
                            data-x="['left','left','left','15','15']" 
                            data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" 
                            data-voffset="['160','160','130','100','100']" 
                            data-fontsize="['16','16','16','16','16']"
                            data-lineheight="['65','65','65','50','35']"
                            data-width="['100%']"
                            data-height="none"
                            data-whitespace="nowarp"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">Digital Transformation</div>
                            
                            <div class="tp-caption tp-resizeme first_text"
                            data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" data-voffset="['215','215','180','150','135']" 
                            data-fontsize="['55','55','55','40','25']"
                            data-lineheight="['68','68','68','50','35']"
                            data-width="['640','640','640','480','300']"
                            data-height="none"
                            data-whitespace="normal"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">Reinventin Companies for a <span>digital future</span></div>
                            
                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['370','370','335','270','220']"  
                                data-fontsize="['18','18','18','16','16']"
                                data-lineheight="['26','26','26','26']"
                                data-width="['570','570','570','550','400']"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text" 
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">O'Prime is one of the world’s top business consulting firms. We help global leaders with their organization's most critical issues and opportunities.
                            </div>
                            
                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['480','480','440','380','350']" 
                                data-fontsize="['14','14','14','14']"
                                data-lineheight="['46','46','46','46']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text" 
                                data-responsive_offset="on" 
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                                <a class="main_btn" href="#">View our services</a>
                            </div>
                        </div>
                    </li>
                    <li data-index="rs-1588" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="img/home-slider/slider-2.png"  data-rotate="0"  data-saveperformance="off"  data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?= site_url()?>/public/site2/img/home-slider/slider-2.png"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            <div class="tp-caption tp-resizeme digit_text"
                            data-x="['left','left','left','15','15']" 
                            data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" 
                            data-voffset="['160','160','130','100','100']" 
                            data-fontsize="['16','16','16','16','16']"
                            data-lineheight="['65','65','65','50','35']"
                            data-width="['100%']"
                            data-height="none"
                            data-whitespace="nowarp"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">Digital Transformation</div>
                            
                            <div class="tp-caption tp-resizeme first_text"
                            data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" data-voffset="['215','215','180','150','135']" 
                            data-fontsize="['55','55','55','40','25']"
                            data-lineheight="['68','68','68','50','35']"
                            data-width="['680','680','680','520','400']"
                            data-height="none"
                            data-whitespace="normal"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">We help B2B companies for their <span>sales growth</span></div>
                            

                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['370','370','335','270','220']"  
                                data-fontsize="['18','18','18','16','16']"
                                data-lineheight="['26','26','26','26']"
                                data-width="['570','570','570','550','400']"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text" 
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">Advotis is one of the world’s top business consulting firms. We help global leaders with their organization's most critical issues and opportunities.
                            </div>
                            
                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['480','480','440','380','350']" 
                                data-fontsize="['14','14','14','14']"
                                data-lineheight="['46','46','46','46']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text" 
                                data-responsive_offset="on" 
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                                <a class="main_btn" href="#">View our services</a>
                            </div>
                        </div>
                    </li>
                    <li data-index="rs-1589" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="<?= site_url()?>/public/site2/img/home-slider/slider-3.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="<?= site_url()?>/public/site2/img/home-slider/slider-3.png"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            <div class="tp-caption tp-resizeme digit_text"
                            data-x="['left','left','left','15','15']" 
                            data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" 
                            data-voffset="['160','160','130','100','100']" 
                            data-fontsize="['16','16','16','16','16']"
                            data-lineheight="['65','65','65','50','35']"
                            data-width="['100%']"
                            data-height="none"
                            data-whitespace="nowarp"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">Digital Transformation</div>
                            
                            <div class="tp-caption tp-resizeme first_text"
                            data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                            data-y="['top','top','top','top']" data-voffset="['215','215','180','150','135']" 
                            data-fontsize="['55','55','55','40','25']"
                            data-lineheight="['68','68','68','50','35']"
                            data-width="['660','660','660','480','300']"
                            data-height="none"
                            data-whitespace="normal"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left']">We provide best <br />quality <span>consulting service</span></div>
                             
                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['370','370','335','270','220']"  
                                data-fontsize="['18','18','18','16','16']"
                                data-lineheight="['26','26','26','26']"
                                data-width="['570','570','570','550','400']"
                                data-height="none"
                                data-whitespace="normal" 
                                data-type="text" 
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">Advotis is one of the world’s top business consulting firms. We help global leaders with their organization's most critical issues and opportunities.
                            </div>
                            
                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['left','left','left','15','15']" data-hoffset="['0','0','0','0']" 
                                data-y="['top','top','top','top']" data-voffset="['480','480','440','380','350']" 
                                data-fontsize="['14','14','14','14']"
                                data-lineheight="['46','46','46','46']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text" 
                                data-responsive_offset="on" 
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]">
                                <a class="main_btn" href="#">View our services</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->
        
        <!--================Our Company Area =================-->
        <section class="our_company_area pad_top">
        	<div class="container">
        		<div class="row our_company_inner">
        			<div class="col-lg-4">
        				<div class="company_sub_title">
        					<h5>Our Company</h5>
        					<h4>We work with you to address your most critical business priorities</h4>
        				</div>
        			</div>
        			<div class="col-lg-7 offset-lg-1">
        				<div class="company_desc">
        					<p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring. </p>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="company_item">
        					<div class="company_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/company/company-1.jpg" alt="">
        					</div>
        					<div class="company_text">
        						<a href="#"><h4>Value Chain Positions</h4></a>
        						<p>Nanotechnology immersion along the information highway will close the loop on focus.</p>
        						<a class="more_btn" href="#">Find our more</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="company_item">
        					<div class="company_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/company/company-2.jpg" alt="">
        					</div>
        					<div class="company_text">
        						<a href="#"><h4>Downside Risks</h4></a>
        						<p>Nanotechnology immersion along the information highway will close the loop on focus.</p>
        						<a class="more_btn" href="#">Find our more</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="company_item">
        					<div class="company_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/company/company-3.jpg" alt="">
        					</div>
        					<div class="company_text">
        						<a href="#"><h4>Key Value Drivers</h4></a>
        						<p>Nanotechnology immersion along the information highway will close the loop on focus.</p>
        						<a class="more_btn" href="#">Find our more</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="company_item">
        					<div class="company_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/company/company-4.jpg" alt="">
        					</div>
        					<div class="company_text">
        						<a href="#"><h4>Investment tenure</h4></a>
        						<p>Nanotechnology immersion along the information highway will close the loop on focus.</p>
        						<a class="more_btn" href="#">Find our more</a>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div class="company_single_text">
        			<p>Quit sitting idle and cash on innovation. <a href="#">Know more about company</a></p>
        		</div>
        	</div>
        </section>
        <!--================End Our Company Area =================-->
        
        <!--================Company Mission Area =================-->
        <section class="company_mission_area p_100">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-6">
        				<div class="mission_img">
        					<img class="img-fluid" src="<?= site_url()?>/public/site2/img/company-mission.jpg" alt="">
        					<div class="company_about">
        						<h4>About Us</h4>
        						<p>Leverage agile frameworks to pro-vide a robust synopsis for high level overviews Iterative</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-6">
        				<div class="mission_text">
        					<h3>Mission of our company to give best service to customers.</h3>
        					<h5>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps nanotechnology immersion along the information.fruit to identify a ballpark value added activity to beta</h5>
        					<p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster ide a robust synopsis for high level overviews. Iterative approaches to corporate strategy focollaborative thinking to further.</p>
        					<a class="more_btn" href="#">Find our more</a>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Company Mission Area =================-->
        
        <!--================Case Area =================-->
        <section class="case_area p_100">
        	<div class="container">
        		<div class="row justify-content-center">
        			<div class="col-lg-3">
        				<div class="case_slider_inner">
							<div class="case_slider owl-carousel">
								<div class="item">
									<h4>Case Studies</h4>
									<p>Bring to the table win-win sur-vival strategies to ensure proac-tive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway healious.</p>
								</div>
								<div class="item">
									<h4>Case Studies</h4>
									<p>Bring to the table win-win sur-vival strategies to ensure proac-tive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway healious.</p>
								</div>
								<div class="item">
									<h4>Case Studies</h4>
									<p>Bring to the table win-win sur-vival strategies to ensure proac-tive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway healious.</p>
								</div>
							</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-6">
        				<div class="case_item">
        					<div class="case_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-1.jpg" alt="">
        					</div>
        					<div class="case_text">
        						<a href="#"><h4>Business Growth</h4></a>
        						<p>Business gworth by strategy of planning.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-6">
        				<div class="case_item">
        					<div class="case_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-2.jpg" alt="">
        					</div>
        					<div class="case_text">
        						<a href="#"><h4>Business Growth</h4></a>
        						<p>Business gworth by strategy of planning.</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-md-6">
        				<div class="case_item">
        					<div class="case_img">
        						<img class="img-fluid" src="<?= site_url()?>/public/site2/img/case/case-3.jpg" alt="">
        					</div>
        					<div class="case_text">
        						<a href="#"><h4>Business Growth</h4></a>
        						<p>Business gworth by strategy of planning.</p>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Case Area =================-->
        
        <!--================Client Slider Area =================-->
        <section class="client_slider_area">
        	<div class="container">
        		<div class="client_slider owl-carousel">
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-1.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-2.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-3.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-4.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-5.png" alt="">
        			</div>
        			<div class="item">
        				<img class="img-fluid" src="<?= site_url()?>/public/site2/img/client/client-1.png" alt="">
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Client Slider Area =================-->
        
        <!--================Testimonials Area =================-->
        <section class="testimonials_area">
        	<div class="container">
        		<div class="single_title">
        			<h2>What our client says</h2>
        		</div>
        		<div class="testi_slider owl-carousel">
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-1.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Marry Devolis</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-2.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Michale John</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-1.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Marry Devolis</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-2.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Marry Devolis</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-1.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Michale John</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="item">
        				<div class="testi_item">
        					<p><span>“</span> Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?= site_url()?>/public/site2/img/testimonials/testi-2.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Marry Devolis</h4>
        							<p>Minda Director</p>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Testimonials Area =================-->
        
        <!--================Feature Content Area =================-->
        <section class="feature_area p_100">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-9">
        				<div class="feature_title_text">
        					<h2>Featured <span>Content</span></h2>
        					<p>By unlocking the potential for growth in our people, clients and our communities we believe we can help shap a vibrant econ-omy for the county</p>
        				</div>
        				<div class="feature_item_inner">
        					<div class="media">
        						<div class="d-flex">
									<img src="<?= site_url()?>/public/site2/img/icon/f-icon-1.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Corporate Strategy</h4>
        							<p>Define the right business to be in and full realise synergies and local & international insight ...</p>
        						</div>
        					</div>
        					<div class="media">
        						<div class="d-flex">
									<img src="<?= site_url()?>/public/site2/img/icon/f-icon-2.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Growth Strategy</h4>
        							<p>Define the right business to be in and full realise synergies and local & international insight ...</p>
        						</div>
        					</div>
        					<div class="media">
        						<div class="d-flex">
									<img src="<?= site_url()?>/public/site2/img/icon/f-icon-3.png" alt="">
        						</div>
        						<div class="media-body">
        							<h4>Business Unit Strategy</h4>
        							<p>Define the right business to be in and full realise synergies and local & international insight ...</p>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3">
        				<div class="feature_person_img">
        					<img src="<?= site_url()?>/public/site2/img/feature/feature-person.png" alt="">
        					<div class="feature_person_text">
        						<h4>Head of <br />Business Consuling</h4>
        						<h5>Michale John</h5>
        						<a href="tel:+18004567890"><h6>+1800 456 7890</h6></a>
        						<a class="mail" href="#">Email <i class="lnr lnr-arrow-right"></i></a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Feature Content Area =================-->
        
        <!--================Industries Area =================-->
        <section class="industries_area pad_top">
        	<div class="container">
        		<div class="main_title">
        			<h2>The Industries That We Serve</h2>
        			<p>Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on  along the information  focusing solely on the bottom line.</p>
        		</div>
        		<div class="industries_key_feature">
					<div class="row">
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-1.png" alt="">
								<h4>Business Growth and Analysis</h4>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-2.png" alt="">
								<h4>Customer <br />Strategy</h4>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-3.png" alt="">
								<h4>Information Technology</h4>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-4.png" alt="">
								<h4>Performance Improvement</h4>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-5.png" alt="">
								<h4>Mergers & <br />Acquisitions</h4>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-6">
							<div class="int_key_item">
								<img src="<?= site_url()?>/public/site2/img/icon/industries-6.png" alt="">
								<h4>Banking & <br />Finances</h4>
							</div>
						</div>
					</div>
        		</div>
        		<a class="more_btn" href="#">view all industries we serve</a>
        		<div class="business_box row m0">
        			<div class="col-lg-6 p0">
						<div class="left_box">
							<h4>How can we build <span>your business?</span></h4>
							<p>We work with you to transform your organization, driving bold ideas and pragmatic solutions.</p>
							<a class="main_btn" href="#">let’s work together</a>
						</div>
        			</div>
        			<div class="col-lg-6 p0">
						<div class="right_box">
							<h4>Our people are our <span>greatest assets</span></h4>
							<p>We work with you to transform your organization, driving bold ideas and pragmatic solutions.</p>
							<a class="white_btn" href="#">come work with us</a>
						</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Industries Area =================-->
        
        <!--================Home Blog Area =================-->
        <section class="home_blog_area pad_btm">
        	<div class="container">
        		<div class="blog_title justify-content-between d-flex">
        			<h2>Insight at advotis Blog</h2>
        			<a href="#">See all posts</a>
        		</div>
        		<div class="row">
        			<div class="col-lg-4">
        				<div class="h_blog_item">
        					<div class="blog_img"><img class="img-fluid" src="<?= site_url()?>/public/site2/img/blog/home-blog/blog-1.jpg" alt=""></div>
        					<div class="blog_text">
        						<a href="#"><h6>Advanced Analytic</h6></a>
        						<a href="<?= site_url()?>/public/site2/single-blog.html"><p>Improve your operational effectiveness and efficiency, and test the results of your customer-facing digital</p></a>
								<a class="date" href="#">October 12, 2018</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4">
        				<div class="h_blog_item">
        					<div class="blog_img"><img class="img-fluid" src="<?= site_url()?>/public/site2/img/blog/home-blog/blog-2.jpg" alt=""></div>
        					<div class="blog_text">
        						<a href="#"><h6>Customer Strategy & Marketing</h6></a>
        						<a href="<?= site_url()?>/public/site2/single-blog.html"><p>Organically grow the holistic world view of disruptive innovation via work.</p></a>
								<a class="date" href="#">December 5, 2018</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4">
        				<div class="h_blog_item">
        					<div class="blog_img"><img class="img-fluid" src="<?= site_url()?>/public/site2/img/blog/home-blog/blog-3.jpg" alt=""></div>
        					<div class="blog_text">
        						<a href="#"><h6>Performance improvement</h6></a>
        						<a href="<?= site_url()?>/public/site2/single-blog.html"><p>Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line</p></a>
								<a class="date" href="#">November 12, 2018</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Home Blog Area =================-->
        
        <!--================End Banner Area =================-->
        <section class="map_area">
            <div id="mapBox" class="mapBox row m0" 
                data-lat="40.701083" 
                data-lon="-74.1522848" 
                data-zoom="13" 
                data-marker="<?= site_url()?>/public/site2/img/map-marker.png"  
                data-info="54B, Tailstoi Town 5238 La city, IA 522364"
                data-mlat="40.701083"
                data-mlon="-74.1522848">
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        <!--================Video Area =================--> 
        <section class="video_area">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-8">
        				<div class="video_text">
        					<h6>Services Insights</h6>
        					<h3>Consulting</h3>
        					<p>Develop advanced analytics strategies that turn your data <br />assets and analytic </p> 
        				</div>
        			</div>
        			<div class="col-lg-4">
        				<div class="video_icon">
        					<a class="popup-youtube" href="https://www.youtube.com/watch?v=uGYd5YqJKPk"><i class="fa fa-play" aria-hidden="true"></i></a>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Video Area =================-->
        
        <!--================Conversion Area =================-->
        <section class="conversion_area">
        	<div class="container">
        		<div class="conversion_inner">
        			<div class="row">
        				<div class="col-lg-6">
        					<div class="conversion_text">
        						<h3>Want to continue the conversion?</h3>
        						<h4>Speak to our expert in</h4>
								<select>
									<option value="1">Select an industry</option>
									<option value="2">Another option</option>
									<option value="3">Potato</option>
								</select>
        					</div>
        				</div>
        				<div class="col-lg-6">
        					<div class="conversion_subs">
        						<p>We help global leaders with their organization's most critical issues and opportunities. Together, we create enduring change and results.</p>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn submit_btn" type="submit" id="button-addon2">contact us</button>
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Conversion Area =================-->
       
        <!--================Search Box Area =================-->
        <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
            <div class="search_box_inner">
                <h3>Search</h3>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Enter search keywords">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <!--================End Search Box Area =================-->
 