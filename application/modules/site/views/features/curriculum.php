<!--==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner features-banner">
            <div class="container">
                <div class="contents">
                    <h1><?= lang('Courses & Curriculum Management')?></h1>
                    <p><?= lang('The purpose of an academic institution is to organize and dispense courses.')?> <span><?= lang('Optimizing curriculum management and dispensation should be of primary importance to the administration.')?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**More Features**
=================================================== -->
        <section class="more-features padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5" style="text-align: justify;">
                        <h2><?= lang('Courses & Curriculum Management')?></h2>
                        <p><?= lang('Institutions face great challenges in managing courses and especially tracking past courses.')?> <?= lang('As new batches are admitted and academic programs are modified, past curriculum records are often mixed up or completely erased. PrimeCampus solves this problem by making courses of each academic batch completely independent from the others while keeping thesame identity.')?> </p>
                         <p><?= lang('With PrimeCampus, Courses can be created semester wise and assigned to lecturers. Students can be given the option to choose courses or take all courses available in that semester.')?></p>
                         <p><?= lang('PrimeCampus makes available a means for lecturers to share their course outline with students and parents. This is meant to keep everyone involved in the progress of the course.')?></p>
                          <p><?= lang('As a batch progresses, it\'s past curriculum record is well preserved so that it can be consulted anytime in the future. Courses and curricullum can also be copied from one past batch to another future batch.')?></p>

                        <p><?= lang('All courses have forums that students can engage in social learning with each other moderated by their lecturers.')?></p>
                        
                    </div>
                    <div class="col-lg-7">
                        <div class="img-holder"><a class="play-btn video" href="https://www.youtube.com/watch?v=3xJzYpRVQVA"><span class="icon-play-btn"></span></a>
                            <figure class="img"><img src="<?= base_url()?>public/c360/images/more-features-img.png" class="img-fluid" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Awsome Design**
=================================================== -->
        <section class="awesome-design padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("Awesome Design & Great Functionalities") ?></h2>
                        <p></p>
                    </div>
                </div>
                <div class="vertical-tab-outer clearfix">
                    <div class="tab-area">
                        <div class="tabs-vertical"> 
                            <!-- Start : tab menu -->
                            <ul>
                                <li class="active" rel="tab-1">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/link-building-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the administrion") ?></h4>
                                        <p><?= lang("Facilitate curriculum planning, creation and review. PrimeCampus stores a history of records that can be consulted at anytime in the future.") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-2" class="">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/seo-succes-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the Lecturers") ?></h4>
                                        <p><?= lang("Facilitates access to course management, content distribution and connection with students.") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-3">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the students") ?></h4>
                                        <p><?= lang("Students are readily updated with their course outlines and have an easy channel to their lecturers. Social learning is possible through the student app as they can engage through course forums.") ?></p>
                                    </div>
                                </li>
                                 <li rel="tab-4">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the parent") ?></h4>
                                        <p><?= lang("Instant access to children academic, disciplinary and financial data. Ease of communication with the administration.") ?></p>
                                    </div>
                                </li>
                            </ul>
                            <!-- End : tab menu --> 
                        </div>
                        <!-- Start : accordion-container -->
                        <div class="tab-vertical-container"> 
                            <!-- Start : #accordion1 -->
                            <div class="tab-drawer-heading active-item" rel="tab-1">
                                <div class="text-div">
                                    <h4><?= lang("Facilitate curriculum planning, creation and review. PrimeCampus stores a history of records that can be consulted at anytime in the future.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-1" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion1 --> 

                            <!-- Start : #accordion2 -->
                            <div class="tab-drawer-heading" rel="tab-2">
                                <div class="text-div">
                                    <h4><?= lang("Facilitates access to course management, content distribution and connection with students.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion2--> 

                            <!-- Start : #accordion3 -->
                            <div class="tab-drawer-heading" rel="tab-3">
                                <div class="text-div">
                                    <h4><?= lang("Students are readily updated with their course outlines and have an easy channel to their lecturers. Social learning is possible through the student app as they can engage through course forums.")?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>


                              <div class="tab-drawer-heading" rel="tab-4">
                                <div class="text-div">
                                    <h4><?= lang("Instant access to children academic, disciplinary and financial data. Ease of communication with the administration.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                           
                            <!-- End : #accordion3 --> 
                        </div>
                        <!-- End :accordion-container --> 
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Counters Sec**
=================================================== -->
        <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("A Better Approach To Academic Management") ?> </h2>
                        <p class="padd-sm"><?= lang("PrimeCampus will give the administration a gobal perception of their institution, by harmonising data from different units and analysing it for better decision making. PrimeCampus will give students, Lectures and parents a better experience in relating to the institution.") ?> </p>
                    </div>
                </div>
               <!--  <ul class="counter-listing">
                    <li>
                        <div class="couter-outer"><span class="counter">190</span><span>+</span></div>
                        <span class="sub-title">Countries</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">700</span><span>M</span></div>
                        <span class="sub-title">Users</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">200</span><span>B</span></div>
                        <span class="sub-title">Calls</span> </li>
                </ul> -->

           
               <!--  <div class="features-carousel-sec">
                    <div class="owl-carousel owl-feature">
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                    </div>
                </div> -->
          
            </div>
        </section>

<!-- ==============================================
**Our Features opt2**
=================================================== -->
        <section class="client-speak our-features padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10"> <span><?= lang("Detailed Features") ?> </span>
                        <h2><?= lang("Features of the Courses and Curricullum Management") ?></h2>
                        <p class="hidden-xs"><?= lang("Outlined below are some of those features that will intrigue you.") ?></p>
                    </div>
                </div>
                <ul class="row features-listing ico-bg">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-analytics"></span></span>
                            <h3><?= lang("Course & Curricullum creation") ?></h3>
                            <p><?= lang("Easily create courses and share curriculum with students and parents.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Learning") ?></h3>
                            <p><?= lang("Facilitate learning through social tools such as forums for students and lecturers.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-responsive"></span></span>
                            <h3><?= lang("Seperate Batch records") ?></h3>
                            <p><?= lang('Keep batch curriculum records seperated from one another to avoid confusion.') ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Copy & Transfer Records") ?></h3>
                            <p><?= lang("Copy curriculum from one batch to the other to facilitate configuration.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-settings"></span></span>
                            <h3><?= lang("Curriculum History") ?></h3>
                            <p><?= lang("Keep a history of course records and curricullum to be consulted at anytime.") ?></p>
                        </div>
                    </li>
                           
                </ul>
            </div>
        </section>

<!-- ==============================================
**Need to Discuss**
=================================================== -->
       <!--  <section class="need-to-discuss bg-img padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 head-block">
                        <h2>Need to Discuss with Us</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
                <div class="submit-form row d-flex">
                    <div class="col-md-6">
                        <input name="First Name" placeholder="First Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Last Name" placeholder="Last Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Company" placeholder="Company" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Phone Number" placeholder="Phone Number" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Business Mail" placeholder="Business Mail" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Job Title" placeholder="Job Title" type="text">
                    </div>
                    <div class="col-md-12">
                        <button class="submit-btn">Submit Now</button>
                    </div>
                </div>
            </div>
        </section> -->

<!-- ==============================================
**Still Have Questains**
=================================================== -->
        <section class="still-hav-qtns-outer gradient-bg padding-lg">
            <div class="container">
                <h2><?= lang("Still have questions?")?></h2>
                <ul class="row features-listing">
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-1.png" alt=""></span>
                            <h3><?= lang("Call us")?></h3>
                            <p><?= lang("For more clarifications, please call any of our numbers below")?></p>
                            <a href="tel:237693527975"  class="know-more">+(237) 693-527-975</a> </div> 
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-2.png" alt=""></span>
                            <h3><?= lang("Leave us a message")?></h3>
                            <p><?= lang("Leave us a message through our contact page and we'll get back to you in at most 48 hours.")?></p>
                            <a href="<?= site_url('contact')?>" class="know-more"><?= lang("Contact us")?></a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-3.png" alt=""></span>
                            <h3><?= lang("Request a demo")?></h3>
                            <p><?= lang("Request a demo to better appreciate the functionalities of  Campus360")?></p>
                            <a href="#" class="know-more">Know more</a> </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->
        <!-- <section class="signup-outer gradient-bg padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <ul class="clearfix">
                            <li> <span class="icon-men"></span>
                                <h4>Signup for an <span>Account</span></h4>
                            </li>
                            <li> <span class="icon-chat"></span>
                                <h4>Discuss with <span>our team</span></h4>
                            </li>
                            <li> <span class="icon-lap"></span>
                                <h4>Receive a <span>good support</span></h4>
                            </li>
                        </ul>
                        <div class="signup-form">
                            <form action="#" method="get">
                                <div class="email">
                                    <input name="email" type="text" placeholder="email">
                                </div>
                                <div class="password">
                                    <input name="password" type="password" placeholder="password">
                                </div>
                                <button class="signup-btn">Sign up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
