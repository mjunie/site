<!--==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner features-banner">
            <div class="container">
                <div class="contents">
                    <h1><?= lang('Student Information Management')?></h1>
                    <p><?= lang('Students are at the heart of every learning institution.')?> <span><?= lang('All management processes within a university unify to give a better experience to students.')?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**More Features**
=================================================== -->
        <section class="more-features padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5" style="text-align: justify;">
                        <h2><?= lang('Student Information Management')?></h2>
                        <p><?= lang('Students are at the heart of every learning institution.')?> <?= lang('All management processes within a university unify to give a better experience to students.')?> </p><p><?= lang('Campus360 focuses on improving the student experience in higher learning by bridging the gap between the students and the administration. Several tools are put in place to collect and manage student data while students are given the opportunity to effectively engage with their institution.')?></p>
                         <p><?= lang('PrimeCampus provides a complete student information system to track students data from the point of admission through graduation. Academic data, finance and disciplinary information is readily available for analysis.')?></p>

                        <p><?= lang('The administration has a general overview of all student data from the instance of admission application through graduation. Such data includes fee payments, attendances, tests and exam marks, results, resits etc. Tools are also put in place to facilitate communication between the administration and students through SM\'s, emails and push notifications. ')?></p>
                        <p><?= lang('Through the students accounts and students mobile app, students get all updates from their schools within an instant. These updates could include, timetables, announcements, results, fee updates and much more.')?></p>
                        
                    </div>
                    <div class="col-lg-7">
                        <div class="img-holder"><a class="play-btn video" href="https://www.youtube.com/watch?v=3xJzYpRVQVA"><span class="icon-play-btn"></span></a>
                            <figure class="img"><img src="<?= base_url()?>public/c360/images/more-features-img.png" class="img-fluid" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Awsome Design**
=================================================== -->
        <section class="awesome-design padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("Awesome Design & Great Functionalities") ?></h2>
                        <p></p>
                    </div>
                </div>
                <div class="vertical-tab-outer clearfix">
                    <div class="tab-area">
                        <div class="tabs-vertical"> 
                            <!-- Start : tab menu -->
                            <ul>
                                <li class="active" rel="tab-1">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/link-building-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the administrion") ?></h4>
                                        <p><?= lang("A general overview of all student data throughout their academic path. Communication and data analysis  tools to optimize management ") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-2" class="">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/seo-succes-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the Lecturers") ?></h4>
                                        <p><?= lang("An overview of all students dat taking their courses, easy communication and lecture dispensation tools.") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-3">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the students") ?></h4>
                                        <p><?= lang("Easy access to school resources, self service tools, instant access to fee data, exams and timetable") ?></p>
                                    </div>
                                </li>
                                 <li rel="tab-4">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the parent") ?></h4>
                                        <p><?= lang("Instant access to children academic, disciplinary and financial data. Ease of communication with the administration.") ?></p>
                                    </div>
                                </li>
                            </ul>
                            <!-- End : tab menu --> 
                        </div>
                        <!-- Start : accordion-container -->
                        <div class="tab-vertical-container"> 
                            <!-- Start : #accordion1 -->
                            <div class="tab-drawer-heading active-item" rel="tab-1">
                                <div class="text-div">
                                    <h4><?= lang("A general overview of all student data throughout their academic path. Communication and data analysis  tools to optimize management ") ?></h4>
                                </div>
                            </div>
                            <div id="tab-1" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion1 --> 

                            <!-- Start : #accordion2 -->
                            <div class="tab-drawer-heading" rel="tab-2">
                                <div class="text-div">
                                    <h4><?= lang("An overview of all students dat taking their courses, easy communication and lecture dispensation tools.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion2--> 

                            <!-- Start : #accordion3 -->
                            <div class="tab-drawer-heading" rel="tab-3">
                                <div class="text-div">
                                    <h4><?= lang("An overview of all students dat taking their courses, easy communication and lecture dispensation tools.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>


                              <div class="tab-drawer-heading" rel="tab-4">
                                <div class="text-div">
                                    <h4><?= lang("Instant access to children academic, disciplinary and financial data. Ease of communication with the administration.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                           
                            <!-- End : #accordion3 --> 
                        </div>
                        <!-- End :accordion-container --> 
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Counters Sec**
=================================================== -->
        <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("A Better Approach To Academic Management") ?> </h2>
                        <p class="padd-sm"><?= lang("PrimeCampus will give the administration a gobal perception of their institution, by harmonising data from different units and analysing it for better decision making. PrimeCampus will give students, Lectures and parents a better experience in relating to the institution.") ?> </p>
                    </div>
                </div>
               <!--  <ul class="counter-listing">
                    <li>
                        <div class="couter-outer"><span class="counter">190</span><span>+</span></div>
                        <span class="sub-title">Countries</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">700</span><span>M</span></div>
                        <span class="sub-title">Users</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">200</span><span>B</span></div>
                        <span class="sub-title">Calls</span> </li>
                </ul> -->

           
               <!--  <div class="features-carousel-sec">
                    <div class="owl-carousel owl-feature">
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                    </div>
                </div> -->
          
            </div>
        </section>

<!-- ==============================================
**Our Features opt2**
=================================================== -->
        <section class="client-speak our-features padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10"> <span><?= lang("Detailed Features") ?> </span>
                        <h2><?= lang("Features of the student information management system") ?></h2>
                        <p class="hidden-xs"><?= lang("Outlined below are some of those features that will intrigue you.") ?></p>
                    </div>
                </div>
                <ul class="row features-listing ico-bg">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-analytics"></span></span>
                            <h3><?= lang("Data Collection & Management") ?></h3>
                            <p><?= lang("Collect and store student  academic, demoraphic data and files for easy access and analysis ") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Data Analysis") ?></h3>
                            <p><?= lang("Analyse students academic data to understand performance and analyse demographic data to enhance marketing efforts") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-responsive"></span></span>
                            <h3><?= lang("Communication") ?></h3>
                            <p><?= lang('Instant communication between administration, students, lecturers and parents through SMS\'S, E-mails, announcements and push notifications') ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Fee Payment") ?></h3>
                            <p><?= lang("Facilitate fee payments through PrimePay integrated directly into student apps.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-settings"></span></span>
                            <h3><?= lang("Results publishing") ?></h3>
                            <p><?= lang("On a sinlge click, publish semester results directly to students and parents mobile phones.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-file"></span></span>
                            <h3><?= lang("Discipline") ?></h3>
                            <p><?= lang("Record and analyse student disciplinary records and instanatly share ith parents.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-graphic"></span></span>
                            <h3><?= lang("Timetable") ?></h3>
                            <p><?= lang("Students can access their timetables directly on their mobile devices and receive daily reminders on course schedules.") ?></p>
                        </div>
                    </li>
                     <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-graphic"></span></span>
                            <h3><?= lang("Transcripts Application") ?></h3>
                            <p><?= lang("Students can apply and pay for their transcripts directly on their mobile devices.") ?></p>
                        </div>
                    </li>
                     <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-graphic"></span></span>
                            <h3><?= lang("Curricullum Dispensation") ?></h3>
                            <p><?= lang("Lecturers can share course program with students directly through their mobile app") ?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Need to Discuss**
=================================================== -->
       <!--  <section class="need-to-discuss bg-img padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 head-block">
                        <h2>Need to Discuss with Us</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
                <div class="submit-form row d-flex">
                    <div class="col-md-6">
                        <input name="First Name" placeholder="First Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Last Name" placeholder="Last Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Company" placeholder="Company" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Phone Number" placeholder="Phone Number" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Business Mail" placeholder="Business Mail" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Job Title" placeholder="Job Title" type="text">
                    </div>
                    <div class="col-md-12">
                        <button class="submit-btn">Submit Now</button>
                    </div>
                </div>
            </div>
        </section> -->

<!-- ==============================================
**Still Have Questains**
=================================================== -->
        <section class="still-hav-qtns-outer gradient-bg padding-lg">
            <div class="container">
                <h2><?= lang("Still have questions?")?></h2>
                <ul class="row features-listing">
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-1.png" alt=""></span>
                            <h3><?= lang("Call us")?></h3>
                            <p><?= lang("For more clarifications, please call any of our numbers below")?></p>
                            <a href="tel:237693527975"  class="know-more">+(237) 693-527-975</a> </div> 
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-2.png" alt=""></span>
                            <h3><?= lang("Leave us a message")?></h3>
                            <p><?= lang("Leave us a message through our contact page and we'll get back to you in at most 48 hours.")?></p>
                            <a href="<?= site_url('contact')?>" class="know-more"><?= lang("Contact us")?></a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-3.png" alt=""></span>
                            <h3><?= lang("Request a demo")?></h3>
                            <p><?= lang("Request a demo to better appreciate the functionalities of  Campus360")?></p>
                            <a href="#" class="know-more">Know more</a> </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->
        <!-- <section class="signup-outer gradient-bg padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <ul class="clearfix">
                            <li> <span class="icon-men"></span>
                                <h4>Signup for an <span>Account</span></h4>
                            </li>
                            <li> <span class="icon-chat"></span>
                                <h4>Discuss with <span>our team</span></h4>
                            </li>
                            <li> <span class="icon-lap"></span>
                                <h4>Receive a <span>good support</span></h4>
                            </li>
                        </ul>
                        <div class="signup-form">
                            <form action="#" method="get">
                                <div class="email">
                                    <input name="email" type="text" placeholder="email">
                                </div>
                                <div class="password">
                                    <input name="password" type="password" placeholder="password">
                                </div>
                                <button class="signup-btn">Sign up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
