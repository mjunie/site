<!--==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner features-banner">
            <div class="container">
                <div class="contents">
                    <h1><?= lang('Finance Management in higher institutions')?></h1>
                    <p><?= lang('All higher institutions are businesses thus their financial state is a key determinant of their growth.')?> <span><?= lang('An easy access to financial data and analytics will go a long way to support decision making and planning.')?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**More Features**
=================================================== -->
        <section class="more-features padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5" style="text-align: justify;">
                        <h2><?= lang('Financial Management')?></h2>
                        <p><?= lang('All higher institutions are businesses thus their financial state is a key determinant of their growth.')?>
                        <?= lang('An easy access to financial data and analytics will go a long way to support decision making and planning.')?></p>

                        <p><?= lang('PrimeCampus provides management with tools to track incomes and expenses from various sources within the institution. All this data can be analyzed to provide periodic financial analysis reports.')?></p>
                         <p><?= lang('Incomes are calculated from various academic fees paid by students, donations and any other source of income the institution may have. Expenses are calculated from salaries, purchases and any other forms of expenses that may arise.')?></p>

                         <p><?= lang('All this data is then organized, analyzed and presented through graphs and balance sheet reports.')?></p>
                         
                        <p><?= lang('An access control system also ensures that only the appropriate staff can have access to financial data. The access control system can control who can view, add or modify any financial data. An activity logging system also ensures that any action performed by a user of the system can be tracked.')?></p>
                        
                    </div>
                    <div class="col-lg-7">
                        <div class="img-holder"><a class="play-btn video" href="https://www.youtube.com/watch?v=3xJzYpRVQVA"><span class="icon-play-btn"></span></a>
                            <figure class="img"><img src="<?= base_url()?>public/c360/images/more-features-img.png" class="img-fluid" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Awsome Design**
=================================================== -->
        <section class="awesome-design padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("Awesome Design & Great Functionalities") ?></h2>
                        <p></p>
                    </div>
                </div>
                <div class="vertical-tab-outer clearfix">
                    <div class="tab-area">
                        <div class="tabs-vertical"> 
                            <!-- Start : tab menu -->
                            <ul>
                                <li class="active" rel="tab-1">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/link-building-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the administrion") ?></h4>
                                        <p><?= lang("A tool to record, track and analyse all financial transactions such as fee payments, salary payments, purchases, donations, expenses. Integration with existing mobile payments and banking services to automate payments.") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-2" class="">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/seo-succes-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the Lecturers") ?></h4>
                                        <p><?= lang("Provides tools for automated salary payments through mobile systems and traceability of financial records.") ?></p>
                                    </div>
                                </li>
                                <li rel="tab-3">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the students") ?></h4>
                                        <p><?= lang("Facilitate fee and services payments through mobile payment methods and a readily available financial records system.") ?></p>
                                    </div>
                                </li>
                                 <li rel="tab-4">
                                    <figure class="icon-div"><img src="<?= base_url()?>public/c360/images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4><?= lang("For the parent") ?></h4>
                                        <p><?= lang("Facilitate fee and services payments through mobile payment methods and a readily available financial records system.") ?></p>
                                    </div>
                                </li>
                            </ul>
                            <!-- End : tab menu --> 
                        </div>
                        <!-- Start : accordion-container -->
                        <div class="tab-vertical-container"> 
                            <!-- Start : #accordion1 -->
                            <div class="tab-drawer-heading active-item" rel="tab-1">
                                <div class="text-div">
                                    <h4><?= lang("A tool to record, track and analyse all financial transactions such as fee payments, salary payments, purchases, donations, expenses. Integration with existing mobile payments and banking services to automate payments.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-1" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion1 --> 

                            <!-- Start : #accordion2 -->
                            <div class="tab-drawer-heading" rel="tab-2">
                                <div class="text-div">
                                    <h4><?= lang("Provides tools for automated salary payments through mobile systems and traceability of financial records.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion2--> 

                            <!-- Start : #accordion3 -->
                            <div class="tab-drawer-heading" rel="tab-3">
                                <div class="text-div">
                                    <h4><?= lang("Facilitate fee and services payments through mobile payment methods and a readily available financial records system.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>


                              <div class="tab-drawer-heading" rel="tab-4">
                                <div class="text-div">
                                    <h4><?= lang("Facilitate fee and services payments through mobile payment methods and a readily available financial records system.") ?></h4>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-vertical-content" >
                                <figure><img src="<?= base_url()?>public/c360/images/awesome-design-img.png" alt=""></figure>
                            </div>
                           
                            <!-- End : #accordion3 --> 
                        </div>
                        <!-- End :accordion-container --> 
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Counters Sec**
=================================================== -->
        <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?= lang("A Better Approach To Academic Management") ?> </h2>
                        <p class="padd-sm"><?= lang("PrimeCampus will give the administration a gobal perception of their institution, by harmonising data from different units and analysing it for better decision making. PrimeCampus will give students, Lectures and parents a better experience in relating to the institution.") ?> </p>
                    </div>
                </div>
               <!--  <ul class="counter-listing">
                    <li>
                        <div class="couter-outer"><span class="counter">190</span><span>+</span></div>
                        <span class="sub-title">Countries</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">700</span><span>M</span></div>
                        <span class="sub-title">Users</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">200</span><span>B</span></div>
                        <span class="sub-title">Calls</span> </li>
                </ul> -->

           
               <!--  <div class="features-carousel-sec">
                    <div class="owl-carousel owl-feature">
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                        <div class="item"><img src="images/counter-sec-img.png"  alt=""></div>
                    </div>
                </div> -->
          
            </div>
        </section>

<!-- ==============================================
**Our Features opt2**
=================================================== -->
        <section class="client-speak our-features padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10"> <span><?= lang("Detailed Features") ?> </span>
                        <h2><?= lang("Features of the Financial management system") ?></h2>
                        <p class="hidden-xs"><?= lang("Outlined below are some of those features that will intrigue you.") ?></p>
                    </div>
                </div>
                <ul class="row features-listing ico-bg">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-analytics"></span></span>
                            <h3><?= lang("Data Collection & Management") ?></h3>
                            <p><?= lang("Collect, track and analyze all financial data related to fee payments, donations, purchases, other expenses etc. ") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Data Analysis") ?></h3>
                            <p><?= lang("Analyze financial data to make more effective decisions.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-responsive"></span></span>
                            <h3><?= lang("Communication") ?></h3>
                            <p><?= lang('Easily communicate financial infos to students, parents and staff through emails, SMS\'s, push notifications etc.') ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3><?= lang("Automated Payments") ?></h3>
                            <p><?= lang("Faciliate payments tuition and services payments for students and pay outs to lecturers through third party systems such as mobile payments and directly to banks.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-settings"></span></span>
                            <h3><?= lang("Finance Records") ?></h3>
                            <p><?= lang("All finance records are readily available to parents and students to boost transparency. Lecturers as well have readily available salary records.") ?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-file"></span></span>
                            <h3><?= lang("Transactions & Balance Sheets") ?></h3>
                            <p><?= lang("Manage transactions between different accounts and generate periodic balance sheets.") ?></p>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </section>

<!-- ==============================================
**Need to Discuss**
=================================================== -->
       <!--  <section class="need-to-discuss bg-img padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 head-block">
                        <h2>Need to Discuss with Us</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
                <div class="submit-form row d-flex">
                    <div class="col-md-6">
                        <input name="First Name" placeholder="First Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Last Name" placeholder="Last Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Company" placeholder="Company" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Phone Number" placeholder="Phone Number" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Business Mail" placeholder="Business Mail" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Job Title" placeholder="Job Title" type="text">
                    </div>
                    <div class="col-md-12">
                        <button class="submit-btn">Submit Now</button>
                    </div>
                </div>
            </div>
        </section> -->

<!-- ==============================================
**Still Have Questains**
=================================================== -->
        <section class="still-hav-qtns-outer gradient-bg padding-lg">
            <div class="container">
                <h2><?= lang("Still have questions?")?></h2>
                <ul class="row features-listing">
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-1.png" alt=""></span>
                            <h3><?= lang("Call us")?></h3>
                            <p><?= lang("For more clarifications, please call any of our numbers below")?></p>
                            <a href="tel:237693527975"  class="know-more">+(237) 693-527-975</a> </div> 
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-2.png" alt=""></span>
                            <h3><?= lang("Leave us a message")?></h3>
                            <p><?= lang("Leave us a message through our contact page and we'll get back to you in at most 48 hours.")?></p>
                            <a href="<?= site_url('contact')?>" class="know-more"><?= lang("Contact us")?></a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="<?= base_url()?>public/c360/images/stil-hav-sec-icon-3.png" alt=""></span>
                            <h3><?= lang("Request a demo")?></h3>
                            <p><?= lang("Request a demo to better appreciate the functionalities of  Campus360")?></p>
                            <a href="#" class="know-more">Know more</a> </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->
        <!-- <section class="signup-outer gradient-bg padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <ul class="clearfix">
                            <li> <span class="icon-men"></span>
                                <h4>Signup for an <span>Account</span></h4>
                            </li>
                            <li> <span class="icon-chat"></span>
                                <h4>Discuss with <span>our team</span></h4>
                            </li>
                            <li> <span class="icon-lap"></span>
                                <h4>Receive a <span>good support</span></h4>
                            </li>
                        </ul>
                        <div class="signup-form">
                            <form action="#" method="get">
                                <div class="email">
                                    <input name="email" type="text" placeholder="email">
                                </div>
                                <div class="password">
                                    <input name="password" type="password" placeholder="password">
                                </div>
                                <button class="signup-btn">Sign up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
