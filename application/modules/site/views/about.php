
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?=  lang('About us')?></h1>
                    <p><?=  lang('A team of young Cameroonians resolved to redefine the academic landscape through effective implementation of digital technology.')?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Who we Are**
=================================================== -->
        <section class="who-we-are padding-lg">
            <div class="container">
                <div class="row row1">
                    <div class="col-lg-5">
                        <figure><img src="<?= base_url()?>public/c360/images/c360.png" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-lg-7">
                        <div class="cnt-block">
                            <h2><?=  lang('Who are we?')?></h2>
                            <p><?=  lang('Campus360 is developed by the team of engineers and consultant at O\'Prime, a software development and technology consulting firm based in Douala.')?></p>

                            <p><?=  lang('Campus360 stems from a series of consulting work done for university institutions in Cameroon. Upon realization of the poor state of technology in these institutions, the team decided to work on a single unified solution for higher education management.')?></p>

                            <p><?=  lang('The system was developed with contributions from university administrators, lecturers and students from english and french speaking public and private universities.')?></p>
                            <p><?=  lang('Campus360 is a highly configurable system that can be easily adapted to any university environment  within the Anglosaxon and French academic systems. Our team will continue to develop our platform to make it more effective for our partner institutions. We aim to;')?></p>

                            <ul class="who-listing">
                                <li><?=  lang('Deliver high quality systems engineering')?></li>
                                <li><?=  lang('Provide continuos support to universities in technology adoption')?></li>
                                <li><?=  lang('Continuos development of our solution to meet growing expectations')?></li>
                                <li><?=  lang('Offer training to students from our partner universities in systems and business engineering')?></li>
                            </ul>
                        </div>
                    </div>
                </div>
               <!--  <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <ul class="counter-listing">
                            <li> 
                                <span class="counter" data-num="2007">2007</span>
                                <span class="sub-title">ESTABLISHED IN</span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="350">350</span><span>+</span></div>
                                <span class="sub-title">CLIENTS WORLD WIDE</span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="950">950</span><span>+</span></div>
                                <span class="sub-title">PROJECTS COMPLETED</span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="100">100</span><span>+</span></div>
                                <span class="sub-title">TEAM MEMBERS</span>
                            </li>
                        </ul>
                    </div>
                </div> -->
            </div>
        </section>


<section class="seo-reports">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 cnt-block">
                        <h2><?= lang('Connecting the dots in higher education')?></h2>
                        <p><?= lang('Higher institutions like any other businesses are complex systems of inter-related units that require proper coordination to ensure growth. Co-ordination here is key to boosting student experience, motivating staff and rewarding investors. C360 understands the many challenges faced by African universities within a local context and provides an effective counter measure. With real time data and analitics on finances, students, courses, examinations, lecturers, more effective decisions can be taken to foster a multilateral growth.')?></p>
                       
                      
                        </div>
                    <div class="col-lg-6">
                        <figure class="img"><img src="<?= base_url()?>public/c360/images/seo-reports.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
            </div>
        </section>
         <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">

                        <h2><?= lang('Redefining the student experience.')?></h2>
                        <p><?= lang('Today\'s student often reffered to as "The connected student", is connected to everything except to his/her institutions. There is a big vacuum in their lifes left unattended to by their institutions. The advantages of leveraging technology to fill this vacuum can be astronimical to both the students and their institutions. C360 offers an amazing suite of mobile and web features to keep students connected to their institution way beyond the school walls. These features will easily boost student experience, facilitate communication,tutoring and attract new fellows.')?></p>

                       
                        <figure class="img"><img src="<?= base_url()?>public/c360/images/generate-forms.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Simple Editor**
=================================================== -->
        <section class="simple-editor padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 order-lg-2 cnt-block">
                         <h2><?= lang('A door way to a better career')?></h2>
                        <p><?= lang('Higher education stands at the cross road into a career for the youths. Universities take on the challenge of guiding youths into a professional life. However many things could easily go wrong. From admission, to course registration and throughout studies,the university is supposed to have a strict eye on every student. But this is not always the case. With a carefully streamlined work-flow, C360 offers African institutions deeper insights into student data. A better understanding of the students strengths and weaknesses would prompt for and adequate counselling into a successfull career.')?></p>
                       </div>
                    <div class="col-lg-7 right"> 
                        <figure class="img"><img src="<?= base_url()?>public/c360/images/simple-editor.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
            </div>
        </section>
<!-- ==============================================
**Take a Tour Section**
=================================================== -->
      <!--   <section class="about-video">
            <div class="container">
                <div class="cnt-block"> <a class="play-btn video" href="https://www.youtube.com/watch?v=3xJzYpRVQVA"><span class="icon-know-more-arrow"></span></a>
                    <h2>Take a tour</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
                </div>
            </div>
        </section> -->

<!-- ==============================================
**Our Team Section**
=================================================== -->
       <!--  <section class="our-team-outer padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <h2>Protech Team</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since beenLorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                    </div>
                </div>
                <ul class="row team-listing">
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img1.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Matt Anderson</h3>
                        <span class="source-title">Development</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing</p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img2.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Pamela Redmond</h3>
                        <span class="source-title">Digital Marketing</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing</p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img3.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Milan Chudoba</h3>
                        <span class="source-title">Co-Founder</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing</p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img4.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Annabel</h3>
                        <span class="source-title">Creative Director</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing</p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </section> -->



<!-- ==============================================
**Footer opt1**
===================================================