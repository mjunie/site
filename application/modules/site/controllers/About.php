<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function about()
	{
		$data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
		$data['page'] = "site/about/about";
		$this->template->site_view($data);
	}

  public function choose()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/about/choose";
      $this->template->site_view($data);

  }
  public function office()
  {
    $data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
    $data['page'] = "site/about/office";
    $this->template->site_view($data);
  }


  public function mission()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/about/mission";
      $this->template->site_view($data);

  }

  public function partners()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/about/partners";
      $this->template->site_view($data);

  }
  public function story()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/about/story";
      $this->template->site_view($data);

  }
  public function testimonial()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/about/testimonial";
      $this->template->site_view($data);

  }
 

}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
