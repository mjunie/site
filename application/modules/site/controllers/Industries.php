<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Industries extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function banking()
	{
		$data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
		$data['page'] = "site/industries/banking";
		$this->template->site_view($data);
	}

  
  public function education()
  {
    $data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
    $data['page'] = "site/industries/education";
    $this->template->site_view($data);
  }


  public function health()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/industries/health";
      $this->template->site_view($data);

  }

  public function hospitality()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/industries/hospitality";
      $this->template->site_view($data);

  }
  public function marketing()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/industries/marketing";
      $this->template->site_view($data);

  }
  public function service()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/industries/service";
      $this->template->site_view($data);

  }
 public function transport()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/industries/transport";
      $this->template->site_view($data);

  }

}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
