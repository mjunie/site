<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		// getting categories
		$data['title'] = lang('University management software');
		$data['desc']  =lang('sitedesc');
		$data['keywords'] =lang('mainkeywords');

						 
						 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function about()
	{
		//getting categories
		$data['title'] = lang('About');
		$data['desc']  =lang('sitedesc');
		$data['keywords'] =lang('mainkeywords');

						 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/about";
		$this->template->site_view($data);
	}



    public function contact(){

		//getting categories
		$data['title'] = lang('Contact');
		$data['desc']  =lang('sitedesc');
		 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/contact";
		$this->template->site_view($data);

    }

    function sendcontact(){

       $email = $this->input->post('email');
       $phone = $this->input->post('phone');
       $name  = $this->input->post('name');
       $institution = $this->input->post('company');
       $message  ="Sender:"." ".$name."<br>";
       $message .="Phone:"." ".$phone."<br>";
       $message .="Email"." ".$email."<br>";
       $message .="Institution"." ".$institution."<br><br>";
       $message .= $this->input->post('message');

    	$this->load->library('email');
    	
    	$this->email->from($email, $name);
    	$this->email->to('info@oprime.net');
    	$this->email->cc('emmanuelnganyu@yahoo.com');
    	$this->email->bcc('info@campus360.cm');
    	
    	$this->email->subject('campus360 Message From'. $name);
    	$this->email->message($message);
    	
    	if($this->email->send()){

    		$this->session->set_flashdata('success', lang("Thank you. Your message has been sent successfully. An agent will get back to you shortly"));
    		redirect(site_url('contact'),'refresh');
    	} else{

    		$this->session->set_flashdata('error', lang("Sorry. Your message was not sent. Please try again"));
    		redirect(site_url('contact'),'refresh');
    	}

    	
    	
 
    }

    function requestademo(){

    	//getting categories
		$data['title'] = lang('Contact');
		$data['desc']  =lang('sitedesc');
		 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/requestademo";
		$this->template->site_view($data);

    }


    function senddemorequest(){

       $email = $this->input->post('email');
       $phone = $this->input->post('phone');
       $name  = $this->input->post('name');
       $institution = $this->input->post('company');
       $message  ="Sender:"." ".$name."<br>";
       $message .="Phone:"." ".$phone."<br>";
       $message .="Email"." ".$email."<br>";
       $message .="Institution"." ".$institution."<br><br>";
       $message .= $this->input->post('message');

    	$this->load->library('email');
    	
    	$this->email->from($email, $name);
    	$this->email->to('info@oprime.net');
    	$this->email->cc('emmanuelnganyu@yahoo.com');
    	$this->email->bcc('info@campus360.cm');
    	
    	$this->email->subject('campus360 Demo Request'. $institution);
    	$this->email->message($message);
    	
    	if($this->email->send()){

    		$this->session->set_flashdata('success', lang("Thank you. Your message has been sent successfully. An agent will get back to you shortly"));
    		redirect(site_url('contact'),'refresh');
    	} else{

    		$this->session->set_flashdata('error', lang("Sorry. Your message was not sent. Please try again"));
    		redirect(site_url('requestdemo'),'refresh');
    	}


    }

	
	
	
}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
