<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Features extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		// getting categories
		$data['title'] = lang('University management software');
		$data['desc']  =lang('sitedesc');
		$data['keywords'] =lang('mainkeywords');

						 
						 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function students()
	{
		//getting categories
		$data['title'] = lang('Student Information Management');
		$data['desc']  =lang('PrimeCampus provides a complete student information system to track students data from the point of admission through graduation. Academic data, finance and disciplinary information is readily available for analysis.');
		$data['keywords'] =lang('mainkeywords');

						 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/features/students";
		$this->template->site_view($data);
	}

  public function staff()
  {
    //getting categories
    $data['title'] = lang('Staff Management');
    $data['desc']  =lang('PrimCampus delivers a set of tools to staffs of all unit in a higher institution to automate tasks. It also provides the administration with means to manage access to informatio and track all activities performes by the staff.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/staff";
    $this->template->site_view($data);
  }


  public function finances()
  {
    //getting categories
    $data['title'] = lang('Financial Management in Higher Institutions');
    $data['desc']  =lang('PrimeCampus provides an accounting system that will properly track all incomes and expenses within a higher institution and generate appropriate reporting for better financial decisions. Expense categories and different income accounts will help the institution streamline financial management and budgeting.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/finance";
    $this->template->site_view($data);
  }

  public function humanresources()
  {
    //getting categories
    $data['title'] = lang('Human Resources & Payroll Management in Higher Institutions');
    $data['desc']  =lang('PrimeCampus provides an automated human resource management system with leave management, attendance, payroll and performance tracking. It automates the payroll process by providing different salary templates, registering payouts and generating payslips.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/humanresource";
    $this->template->site_view($data);
  }


   public function timetable()
  {
    //getting categories
    $data['title'] = lang('Timetable Management');
    $data['desc']  =lang('PrimeCampus provides a tool to easily create and share timetables among students and lecturers. The timetable also enforces attendance registration.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/timetable";
    $this->template->site_view($data);
  }



     public function assets()
  {
    //getting categories
    $data['title'] = lang('Assets Management');
    $data['desc']  =lang('PrimeCampus provides a tool to easily track and manage all assets belonging to the institution. This tool tracks movement of assets, depreciation and can help compute the material worth of an institution at every given time.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/assets";
    $this->template->site_view($data);
  }


     public function store()
  {
    //getting categories
    $data['title'] = lang('Store Management');
    $data['desc']  =lang('For institutions with shops where students and other personelle could purchase items, PrimeCampus provides them a complete retail and inventory management system to track sales of items and inventory.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/assets";
    $this->template->site_view($data);
  }

       
       public function curriculum()
  {
    //getting categories
    $data['title'] = lang('Courses & Curriculum Management');
    $data['desc']  = lang('PrimeCampus offers a structured approach to curriculum management by facillitating creation of courses, course assignment to programs and lecturers, course sysllables and course history. The batch based approach ensures that the same courses support different configurations through different batches.');
    $data['keywords'] =lang('mainkeywords');

             $this->db->limit(3);
             $this->db->order_by('id','DESC');
             $data['posts'] =$this->db->get('blog_post')->result();
    $data['page'] = "site/features/curriculum";
    $this->template->site_view($data);
  }

    public function contact() 
  {

		//getting categories
		$data['title'] = lang('Contact');
		$data['desc']  =lang('sitedesc');
		 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/contact";
		$this->template->site_view($data);
  }

    function sendcontact(){

       $email = $this->input->post('email');
       $phone = $this->input->post('phone');
       $name  = $this->input->post('name');
       $institution = $this->input->post('company');
       $message  ="Sender:"." ".$name."<br>";
       $message .="Phone:"." ".$phone."<br>";
       $message .="Email"." ".$email."<br>";
       $message .="Institution"." ".$institution."<br><br>";
       $message .= $this->input->post('message');

    	$this->load->library('email');
    	
    	$this->email->from($email, $name);
    	$this->email->to('info@oprime.net');
    	$this->email->cc('emmanuelnganyu@yahoo.com');
    	$this->email->bcc('info@campus360.cm');
    	
    	$this->email->subject('campus360 Message From'. $name);
    	$this->email->message($message);
    	
    	if($this->email->send()){

    		$this->session->set_flashdata('success', lang("Thank you. Your message has been sent successfully. An agent will get back to you shortly"));
    		redirect(site_url('contact'),'refresh');
    	} else{

    		$this->session->set_flashdata('error', lang("Sorry. Your message was not sent. Please try again"));
    		redirect(site_url('contact'),'refresh');
    	}

    	
    	
 
    }

    function requestademo(){

    	//getting categories
		$data['title'] = lang('Contact');
		$data['desc']  =lang('sitedesc');
		 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();
		$data['page'] = "site/requestademo";
		$this->template->site_view($data);

    }


    function senddemorequest(){

       $email = $this->input->post('email');
       $phone = $this->input->post('phone');
       $name  = $this->input->post('name');
       $institution = $this->input->post('company');
       $message  ="Sender:"." ".$name."<br>";
       $message .="Phone:"." ".$phone."<br>";
       $message .="Email"." ".$email."<br>";
       $message .="Institution"." ".$institution."<br><br>";
       $message .= $this->input->post('message');

    	$this->load->library('email');
    	
    	$this->email->from($email, $name);
    	$this->email->to('info@oprime.net');
    	$this->email->cc('emmanuelnganyu@yahoo.com');
    	$this->email->bcc('info@campus360.cm');
    	
    	$this->email->subject('campus360 Demo Request'. $institution);
    	$this->email->message($message);
    	
    	if($this->email->send()){

    		$this->session->set_flashdata('success', lang("Thank you. Your message has been sent successfully. An agent will get back to you shortly"));
    		redirect(site_url('contact'),'refresh');
    	} else{

    		$this->session->set_flashdata('error', lang("Sorry. Your message was not sent. Please try again"));
    		redirect(site_url('requestdemo'),'refresh');
    	}


    }

	
	
	
}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
