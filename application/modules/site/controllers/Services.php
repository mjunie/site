<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Services extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function amazon()
	{
		$data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
		$data['page'] = "site/services/computing/amazon";
		$this->template->site_view($data);
	}

  public function azur()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/computing/azur";
      $this->template->site_view($data);

  }
  public function digital()
  {
    $data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
    $data['page'] = "site/services/computing/digital";
    $this->template->site_view($data);
  }


  public function google()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/computing/google";
      $this->template->site_view($data);

  }

  public function infrastructure()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/computing/infrast";
      $this->template->site_view($data);

  }
  public function helpdesk()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/corporate/helpdesk";
      $this->template->site_view($data);

  }
  public function microsoft()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/corporate/microsoft";
      $this->template->site_view($data);

  }
  public function development()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/engineering/development";
      $this->template->site_view($data);

  }
  public function integration()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/engineering/integration";
      $this->template->site_view($data);

  }
  public function modelling_engineering()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/engineering/modelling";
      $this->template->site_view($data);

  }
 public function business()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/erp/business";
      $this->template->site_view($data);

  }
  public function crm()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/erp/crm";
      $this->template->site_view($data);

  }
  public function erp()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/erp/erp";
      $this->template->site_view($data);

  }
  public function hrm()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/erp/hrm";
      $this->template->site_view($data);

  }
public function database()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/intelligence/database";
      $this->template->site_view($data);

  }
public function intelligence()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/intelligence/intelligence";
      $this->template->site_view($data);

  }
public function modelling()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/intelligence/modelling";
      $this->template->site_view($data);

  }
public function policies()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/security/policies";
      $this->template->site_view($data);

  }
public function risk()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/security/risk";
      $this->template->site_view($data);

  }
public function webapp()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/services/security/webapp";
      $this->template->site_view($data);

  }

}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
