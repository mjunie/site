<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Solutions extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

	
	}

	/**
	 * [Load Categories]
	 * @return [void]
	 */
	public function index()
	{
		
		
		$data['page'] = "site/index";
		$this->template->site_view($data);
	}

	public function primeone()
	{
		$data['title'] = lang('primepos title');
    $data['desc']  =lang('PrimCampus delivers');  
		$data['page'] = "site/solutions/business/primeone";
		$this->template->site_view($data);
	}

  public function primepos()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/business/primepos";
      $this->template->site_view($data);

  }

  public function primesms()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/business/primesms";
      $this->template->site_view($data);

  }

  public function alumnia()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/alumnia";
      $this->template->site_view($data);

  }

  public function apson()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/apson";
      $this->template->site_view($data);

  }

  public function certverify()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/certverify";
      $this->template->site_view($data);

  }
public function iapply()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/iapply";
      $this->template->site_view($data);

  }

public function primecampus()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/primecampus";
      $this->template->site_view($data);

  }

  public function revise()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/education/revise";
      $this->template->site_view($data);

  }
public function events()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/events/eventa";
      $this->template->site_view($data);

  }

public function mifosx()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/finance/mifosx";
      $this->template->site_view($data);

  }
public function primefinx()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/finance/primefinx";
      $this->template->site_view($data);
  }

public function primepay()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/finance/primepay";
      $this->template->site_view($data);

  }

  
public function primecms()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/general/primecms";
      $this->template->site_view($data);

  }

public function primehotel()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/hospitality/primehotel";
      $this->template->site_view($data);

  }

public function apson_social()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/social/apson";
      $this->template->site_view($data);

  }
  
public function primecx()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/social/primecx";
      $this->template->site_view($data);

  }
  
public function primetlms()
  {
     $data['title'] = lang('primepos title');
     $data['desc']  =lang('PrimCampus delivers');  

      $data['page'] = "site/solutions/transport/primetlms";
      $this->template->site_view($data);

  }


}

/* End of file BlogCategory.php */
/* Location: ./application/modules/blog/controllers/BlogCategory.php */
