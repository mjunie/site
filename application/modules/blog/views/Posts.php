<link rel="stylesheet" type="text/css" href="<?= bs() ?>public/b-asset/lib/summernote/summernote-bs4.css"/>

<div class="be-content">
   <div class="page-head">
      <h2 class="page-head-title">Blog Posts</h2>
      <nav aria-label="breadcrumb" role="navigation">
         <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Blogs</a></li>
            <li class="breadcrumb-item active">Add Blog Post</li>
         </ol>
      </nav>
   </div>
   <div class="main-content container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <?php if (!empty($this->session->flashdata('success'))) : ?>
               <div class="alert alert-contrast alert-success alert-dismissible" role="alert">
                  <div class="icon"><span class="mdi mdi-check"></span></div>
                  <div class="message">
                     <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                     <strong> <?= $this->session->flashdata('success') ?> </strong>
                  </div>
               </div>
            <?php endif; ?>
            <?php if (!empty(validation_errors())) : ?>

               <div class="alert alert-contrast alert-danger alert-dismissible" role="alert">
                  <div class="icon"><span class="mdi mdi-check"></span></div>
                  <div class="message">
                     <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                     <strong> <?php echo validation_errors(); ?></strong>
                  </div>
               </div>

            <?php endif ?>
            <div class="card card-table  card-border-color card-border-color-primary">
               <div class="card-header"> <span class="icon mdi mdi-blogger"></span> Add Blog Post

               </div>
               <div class="card-body">
                  <div class="row ml-5">
                     <div class="col-sm-12">
                        <form action="<?= base_url('blog/Posts/add_post') ?>" enctype="multipart/form-data" method="post" class="form-horizontal row-border">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-5">
                                    <label class="control-label">
                                       Post Title
                                    </label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Post Title" required>
                                 </div>
                                 <div class="col-sm-5">
                                    <label class="control-label">Select Category </label>
                                    <select name="category" class="form-control" required>
                                       <option value="">Select Category</option>
                                       <?php foreach ($categories as $category) : ?>
                                          <option value="<?php echo $category->id ?>"><?php echo $category->cat_name ?></option>
                                       <?php endforeach ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-5">
                                    <label class="control-label">Meta Description</label>
                                    <textarea name="meta_desc" class="form-control" rows="5" placeholder="Description Must be within 150 characters" required></textarea>
                                 </div>
                                 <div class="col-sm-5">
                                    <label class="control-label">Meta Keywords</label>
                                    <textarea name="meta_keyword" class="form-control" rows="5" placeholder="keyword1,keyword2 etc" required></textarea>
                                    <p class="help-block">Please Separate Keywords with comma ",".</p>
                                 </div>
                              </div>
                           </div>

                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-5">
                                    <label class="control-label">Select Tags</label>
                                    <select name="tags[]" class="form-control" required multiple>
                                       <?php foreach ($tags as $tag) : ?>
                                          <option value="<?php echo $tag->id ?>"><?php echo $tag->tag ?></option>
                                       <?php endforeach ?>
                                    </select>
                                    <p class="help-block">Press CTRL to select multiple tags</p>
                                 </div>
                                 <div class="col-sm-5">
                                    <label class="control-label">Featured Image</label>
                                    <label for="blog_img">
                                       <img src="<?php bs() ?>public/b-asset/img/342x239.png" id="img" width="100" height="100">
                                    </label>
                                    <input id="blog_img" name="img" type="file" class="blog_img visible" style="display: none;" >
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-5">
                                    <label class="control-label">Status </label>
                                    <select name="status" class="form-control" required>
                                       <option value="1">Publish</option>
                                       <option value="0">Unpublish</option>
                                    </select>
                                 </div>
                                 <div class="col-sm-5">
                                    <label class="control-label">Allow Comments </label>
                                    <select name="allow_comment" class="form-control" required>
                                       <option value="1">Enable</option>
                                       <option value="0">Disable</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-10">
                                    <label class="control-label">Post Content </label>
                                    <textarea name="content" id="editor1" class="form-control" rows="5"></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label"></label>
                              <div class="col-sm-12 text-center">
                                 <button type="submit" class="btn btn-primary"> <span class="icon mdi mdi-blogger"></span> Save</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script>
      function first_img(input) {

         if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
               $('#img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
         }
      }

      $(".blog_img").change(function() {
         first_img(this);
      });
   </script>