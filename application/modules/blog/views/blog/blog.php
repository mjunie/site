<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?=lang('Blog, News and Events') ?></h1>
                    <p><?=lang('Read the latest about  Campus360, Get informed and stay connected!') ?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Blog section**
=================================================== -->
        <section class="blog-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="blog-content masonry-blog">
                            <ul class="clearfix">



                               <?php if (empty($pposts)): ?>
                    
                               <img src="<?php echo $setting->default_img ?>" alt="default image" width="100%">

                               <?php else: ?>  

                               <?php foreach ($pposts as $post): ?>

                                <li>
                                    <div class="blog-item">
                                        <figure class="blog-pic"><img class="img-fluid" src="<?= $post->featured_img ?>" alt=""></figure>
                                        <p class="time"><?php echo date("M d", strtotime($post->created_at)) ?><span><?php echo date("Y", strtotime($post->created_at)) ?></span></p>
                                        <h5><a href="<?php bs() ?>blog/post/<?php echo $post->slug ?>"> <?php echo $post->title ?>.</a></h5>
                                      
                                        
                                       
                                    </div>
                                </li>
                        
                   

                <?php endforeach ?>

                <?php endif ?>  



                              
                             
                            </ul>
                            <div class="paging-block">
                               
                                    <?php if(isset($links)){ echo $links;} ?>
                               
                            </div>
                        </div>
                    </div>

<!-- ==============================================
**Blog Sidebar**
============================================== -->
                    <aside class="col-md-4 col-lg-3">
                        <div class="blog-sidebar"> 

                            <!--Start Categories-->
                            <div class="cmn-box">
                                <h4><?= lang('Categories');?></h4>
                                 <?php if (empty($categories)): ?>
                                        
                                        <a href="#"><?= lang('Categories are not available');?></a>

                                    <?php else: ?>  

                                <ul>

                                <?php foreach ($categories as $category): ?>

                                        <?php if ($setting->enable_category_posts == 0): ?>
                                            
                                            <a href="#"><font color="red"><?= lang('Categories Posts Disable by Admin.');?></font></a>

                                            <?php break; ?>

                                        <?php else: ?>
                                            
                                        <li><a href="<?php bs() ?>blog/category/<?php echo $category->slug ?>"><?php echo $category->cat_name ?><span class="count"><?php echo countCategoryPost($category->id) ?></span></a></li>

                                        <?php endif ?>

                                    <?php endforeach ?> 

                               
                            </ul>
                             <?php endif ?>
                                   
                            </div>
                            <!--End Categories--> 

                           

                            <!--Start Recent Articles-->
                            <div class="cmn-box archive">
                                <h4><?= lang('Recent Articles');?></h4>



                                <?php if (empty($recent_posts)): ?>
                                    
                                    <a href="#"><?= lang('Recent Articles Not Available.');?></a>

                                <?php else: ?>  

                                <?php foreach ($recent_posts as $recent): ?>

                                    <?php if ($setting->enable_recent_post == 0): ?>
                                        
                                        <a href="#"><font color="red"><?= lang('Recent Posts Disable by Admin.');?></font></a>

                                        <?php break; ?>

                                 <?php else: ?>

                                 <div class="article-box">
                                    <figure class="article-pic"><a href="<?php bs() ?>blog/post/<?php echo $recent->slug ?>" class="overlay-link"><img class="img-fluid" src="<?= $recent->featured_img ?>" alt=""></a></figure>
                                    <a href="<?php bs() ?>blog/post/<?php echo $recent->slug ?>"><p><?php echo $recent->title ?></p></a>
                                    <p class="time"> <?php echo date("M d", strtotime($recent->created_at)) ?><span> <?php echo date("Y", strtotime($recent->created_at)) ?></span></p>
                                 </div>

                                <?php endif ?>

                                <?php endforeach ?> 

                                <?php endif ?>

                            </div>
                            <!--End Recent Articles--> 

                            <!--Start Tags-->
                            <div class="cmn-box tags">
                                <h4>Tags</h4>


                                <?php if (empty($tags)): ?>
                                    
                                <a href="#">Tags Not Define</a>

                            <?php else: ?>      
                            <ul class="blog-tag">
                                <?php foreach ($tags as $tag): ?>
                                    
                                    <li><a href="<?php bs() ?>blog/tags/<?php echo $tag->slug ?>"><?php echo $tag->tag ?></a></li>

                                <?php endforeach ?>
                                </ul>
                            <?php endif ?>   
                               
                                
                            </div>
                            <!--End Tags--> 

                        </div>
                    </aside>
                </div>
            </div>
        </section>


