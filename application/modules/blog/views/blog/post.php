<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $post->title ?></h1>
                    <p></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Blog section**
=================================================== -->
        <section class="blog-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="blog-content blog-detailed">
                            <figure class="blog-pic"><img class="img-fluid" src="<?php echo $post->featured_img ?>" alt=""></figure>
                            <p class="time"><?php echo date("M d,", strtotime($post->created_at)) ?>  <span><?php echo date("Y", strtotime($post->created_at)) ?></span></p>
                            <h5><?php echo $post->title ?></h5>
                           
                            <p> <?php echo $post->content ?></p>
                            
                            <div class="disqus-cmt">
                                <div id="disqus_thread"></div>
                                <script>

                                    /**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                    /*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
                                    (function() { // DON'T EDIT BELOW THIS LINE
                                        var d = document, s = d.createElement('script');
                                        s.src = '../../http-protechtheme-com.disqus.com/embed.js';
                                        s.setAttribute('data-timestamp', +new Date());
                                        (d.head || d.body).appendChild(s);
                                    })();
                                </script>
                                <noscript>
                                    Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
                                </noscript>
                            </div>
                        </div>
                    </div>

<!-- ==============================================
**Blog Sidebar**
============================================== -->
                    <aside class="col-md-4 col-lg-3">
                        <div class="blog-sidebar"> 

                            <!--Start Categories-->
                            <div class="cmn-box">
                                <h4><?= lang('Categories');?></h4>
                                 <?php if (empty($categories)): ?>
                                        
                                        <a href="#"><?= lang('Categories are not available');?></a>

                                    <?php else: ?>  

                                <ul>

                                <?php foreach ($categories as $category): ?>

                                        <?php if ($setting->enable_category_posts == 0): ?>
                                            
                                            <a href="#"><font color="red"><?= lang('Categories Posts Disable by Admin.');?></font></a>

                                            <?php break; ?>

                                        <?php else: ?>
                                            
                                        <li><a href="<?php bs() ?>blog/category/<?php echo $category->slug ?>"><?php echo $category->cat_name ?><span class="count"><?php echo countCategoryPost($category->id) ?></span></a></li>

                                        <?php endif ?>

                                    <?php endforeach ?> 

                               
                            </ul>
                             <?php endif ?>
                                   
                            </div>
                            <!--End Categories--> 

                           

                            <!--Start Recent Articles-->
                            <div class="cmn-box archive">
                                <h4><?= lang('Recent Articles');?></h4>



                                <?php if (empty($recent_posts)): ?>
                                    
                                    <a href="#"><?= lang('Recent Articles Not Available.');?></a>

                                <?php else: ?>  

                                <?php foreach ($recent_posts as $recent): ?>

                                    <?php if ($setting->enable_recent_post == 0): ?>
                                        
                                        <a href="#"><font color="red"><?= lang('Recent Posts Disable by Admin.');?></font></a>

                                        <?php break; ?>

                                 <?php else: ?>

                                 <div class="article-box">
                                    <figure class="article-pic"><a href="<?php bs() ?>blog/post/<?php echo $recent->slug ?>" class="overlay-link"><img class="img-fluid" src="<?= $recent->featured_img ?>" alt=""></a></figure>
                                    <a href="<?php bs() ?>blog/post/<?php echo $recent->slug ?>"><p><?php echo $recent->title ?></p></a>
                                    <p class="time"> <?php echo date("M d", strtotime($recent->created_at)) ?><span> <?php echo date("Y", strtotime($recent->created_at)) ?></span></p>
                                 </div>

                                <?php endif ?>

                                <?php endforeach ?> 

                                <?php endif ?>

                            </div>
                            <!--End Recent Articles--> 

                            <!--Start Tags-->
                            <div class="cmn-box tags">
                                <h4>Tags</h4>


                                <?php if (empty($tags)): ?>
                                    
                                <a href="#">Tags Not Define</a>

                            <?php else: ?>      
                            <ul class="blog-tag">
                                <?php foreach ($tags as $tag): ?>
                                    
                                    <li><a href="<?php bs() ?>blog/tags/<?php echo $tag->slug ?>"><?php echo $tag->tag ?></a></li>

                                <?php endforeach ?>
                                </ul>
                            <?php endif ?>   
                               
                                
                            </div>
                            <!--End Tags--> 

                        </div>
                    </aside>
                </div>
            </div>
        </section>
