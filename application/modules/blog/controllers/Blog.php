<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller 
{
	public $setting;

	public function __construct()
	{
		parent::__construct();
		$this->load->module('template');
		$this->load->model('common_model');
		$this->load->library('form_validation');

		$this->load->model('common_model');

		$this->setting = $this->common_model->getAllData('blog_settings','*','1'); 	
	}

	/**
	 * [All Posts]
	 * @return [void]
	 */
	public function index()
	{
		$this->load->library('pagination');

		/* Getting per page post for settings table */
        $per_page_posts = $this->common_model->getAllData('blog_settings','per_page_posts','1');

        /* count all rows in blog posts table */
		$total_rows = $this->db->count_all("blog_post");

		$config = array();
        $config["base_url"] = base_url() . "blog/blog/index";
        $config["total_rows"] = $total_rows;
        $config["per_page"] = empty($per_page_posts->per_page_posts) ? 8 : $per_page_posts->per_page_posts;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
    	$config["num_links"] = round($choice);

    	$config['full_tag_open']    = "<ul>";
		$config['full_tag_close']   ="</ul>";
		$config['num_tag_open']     = '<li>';
		$config['num_tag_close']    = '</li>';
		$config['cur_tag_open']     = "<li><a class='active' href='#0'>";
		$config['cur_tag_close']    = "</a></li>";
		$config['next_tag_open']    = "<li>";
		$config['next_tagl_close']  = "</li>";
		$config['prev_tag_open']    = "<li>";
		$config['prev_tagl_close']  = "</li>";
		$config['first_tag_open']   = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open']    = "<li>";
		$config['last_tagl_close']  = "</li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data["pposts"] = $this->common_model->
            fetch_posts($config["per_page"], $page);

						 $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();

		$data['tags'] = $this->common_model->getAllData('blog_tags','*');

        $data['recent_posts'] = $this->common_model->getAllData('blog_post','id,title,slug,featured_img,created_at','','','id  desc','5');

        $data['categories'] = $this->common_model->getAllData('blog_categories','id,cat_name,slug','','','id  desc','5');

        $data["links"] = $this->pagination->create_links();

        $data['setting'] = $this->setting;

		$data['title'] = lang("Blog, News and Events");
		$data['page']    ="blog/blog/blog";
		$this->template->site_view($data);
	}

	/**
	 * [Single Post]
	 * @return [void]
	 */
	public function single_post()
	{
		$title = $this->uri->segment(3);

		$data['post'] = $this->common_model->DJoin('*,blog_post.id AS post_id','blog_post','users','blog_post.user_id = users.id','1','',array('slug' => $title));

		$data['prev'] = $this->common_model->getAllData('blog_post','slug','1',array('id <'=> $data['post']->post_id),'id desc');

		$data['next'] = $this->common_model->getAllData('blog_post','slug','1',array('id >'=> $data['post']->post_id));

        $data['setting'] = $this->setting;

        $data['setting'] = $this->setting;

        $data['tags'] = $this->common_model->getAllData('blog_tags','*');

        $data['recent_posts'] = $this->common_model->getAllData('blog_post','id,title,slug,featured_img,created_at','','','id  desc','5');
        $data['categories'] = $this->common_model->getAllData('blog_categories','id,cat_name,slug','','','id  desc','5');

        $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();

		$data['title'] = $data['post']->title;
		$data['page']    ="blog/blog/post";
		$this->template->site_view($data);
	}

	/**
	 * [Get All data related to tags]
	 * @return [type] [description]
	 */
	public function tags()
	{

		$title = $this->uri->segment(3);

		$tags = array('blog_post' => 'blog_post.id = blog_post_tags.post_id');

		$data['setting'] = $this->setting;

		 $data['tags'] = $this->common_model->getAllData('blog_tags','*');

         $data['recent_posts'] = $this->common_model->getAllData('blog_post','id,title,slug,featured_img,created_at','','','id  desc','5');

        $data['categories'] = $this->common_model->getAllData('blog_categories','id,cat_name,slug','','','id  desc','5');

		$data['pposts'] = $this->common_model->DJoin('*','blog_tags','blog_post_tags','blog_tags.id = blog_post_tags.tag_id','',$tags,array('blog_tags.slug' => $title));

		$this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();

        $data['title'] =    $this->db->get_where('blog_tags',array('slug'=>$title))->row()->tag;
		$data['page']    ="blog/blog/blog";
		$this->template->site_view($data);
	}

	/**
	 * [Get All data to Categories]
	 * @return [void]
	 */
	public function categories()
	{		
		$category = $this->uri->segment(3);

		$data['pposts'] = $this->common_model->DJoin('*','blog_categories','blog_post','blog_categories.id = blog_post.category_id','','',array('blog_categories.slug' => $category));

		$data['setting'] = $this->setting;

		$data['tags'] = $this->common_model->getAllData('blog_tags','*');

         $data['recent_posts'] = $this->common_model->getAllData('blog_post','id,title,slug,featured_img,created_at','','','id  desc','5');

        $data['categories'] = $this->common_model->getAllData('blog_categories','id,cat_name,slug','','','id  desc','5');

        $this->db->limit(3);
						 $this->db->order_by('id','DESC');
						 $data['posts'] =$this->db->get('blog_post')->result();

		$data['title'] = $this->db->get_where('blog_categories',array('slug'=>$category))->row()->cat_name;
		$data['page']  ="blog/blog/blog";
		$this->template->site_view($data);
	}

	public function search()
	{
		$keyword = $this->input->get('search');

		$data['search'] = $this->common_model->getAllData('blog_post','*','','','','','',array('title' => $keyword));

		$data['setting'] = $this->setting;

		// pr($data);

		view('talos/search',$data);

	}

	
}

/* End of file BlogFrontend.php */
/* Location: ./application/modules/blog/controllers/BlogFrontend.php */