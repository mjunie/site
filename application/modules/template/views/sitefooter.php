 <!--================Footer Area =================-->
 <footer class="footer_area">
        	<div class="footer_top">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-3 col-sm-6">
        					<aside class="f_widget ab_widget">
        						<img src="<?= site_url()?>/public/site2/img/logo.png"  height="55px" width="153px" alt="">
        						<p>854 Lorance Road, Rose Vallery, <br /> Orlando, New York 8564, <br /> United States.</p>
        						<h6>Talk to an expert </h6>
        						<a href="tel:18004567890">1800 456 7890</a>
        					</aside>
        				</div>
        				<div class="col-lg-5 col-sm-6">
        					<aside class="f_widget link_widget">
        						<div class="f_title">
									<h3>Useful Links</h3>
        							<span></span>
        						</div>
        						<ul class="nav flex-column">
        							<li><a href="#">Home</a></li>
        							<li><a href="#">All Industries</a></li>
        							<li><a href="#">Case Studies</a></li>
        							<li><a href="#">Insights</a></li>
        							<li><a href="#">Testimonials</a></li>
        						</ul>
        						<ul class="nav flex-column">
        							<li><a href="#">Meet Our Team</a></li>
        							<li><a href="#">About Us</a></li>
        							<li><a href="#">Mission & Vision</a></li>
        							<li><a href="#">Our Partners</a></li>
        							<li><a href="#">Contact Us</a></li>
        						</ul>
        					</aside>
        				</div>
        				<div class="col-lg-4 col-sm-6">
        					<aside class="f_widget news_widget">
        						<div class="f_title">
        							<h3>Newsletter</h3>
        							<span></span>
        						</div>
        						<p>Get latest updates and offers.</p>
        						<div class="input-group">
									<input type="text" class="form-control" placeholder="Your email address" aria-label="Recipient's username" aria-describedby="button-addon2">
									<div class="input-group-append">
									<button class="btn" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
									</div>
								</div>
       							<ul class="nav">
       								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
       								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
       								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
       								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
       							</ul>
        					</aside>
        				</div>
        			</div>
        		</div>
        	</div>
        	<div class="footer_bottom">
        		<div class="container">
        			<div class="justify-content-between d-flex">
        				<div class="left">
        					<p>© Copyright  Advotis Firm <script>document.write(new Date().getFullYear());</script> . All right reserved.</p>
        				</div>
        				<div class="right">
        					<p>Created by <a href="#">DesignArc</a></p>
        				</div>
        			</div>
        		</div>
        	</div>
        </footer>
        <!--================End Footer Area =================-->
        
        <!--================Search Box Area =================-->
        <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
            <div class="search_box_inner">
                <h3>Search</h3>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Enter search keywords">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <!--================End Search Box Area =================-->
        
        
        
        
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?= site_url()?>/public/site2/js/jquery-3.3.1.min.js"></script>
        <script src="<?= site_url()?>/public/site2/js/popper.min.js"></script>
        <script src="<?= site_url()?>/public/site2/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra Plugin js -->
        <script src="<?= site_url()?>/public/site2/vendors/nice-selector/js/jquery.nice-select.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/popup/jquery.magnific-popup.min.js"></script>
        <script src="<?= site_url()?>/public/site2/vendors/nice-selector/js/jquery.nice-select.min.js"></script>
        
        <!--gmaps Js-->
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBvEEMx3XDpByNzYNn0n62Zsq_sVYPx1zY&amp;ver=5.2.1"></script>
        <script src="<?= site_url()?>/public/site2/js/gmaps.min.js"></script>
        <script src="<?= site_url()?>/public/site2/js/map-active.js"></script>
        
        <script src="<?= site_url()?>/public/site2/js/theme.js"></script>
    </body>

<!-- Mirrored from designarc.biz/demos/advotis/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 May 2020 12:52:38 GMT -->
</html>