<!doctype html>
<html lang="en">
    
<!-- Mirrored from designarc.biz/demos/advotis/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 May 2020 12:50:30 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="<?= site_url()?>/public/site2/img/favicon.png" type="image/png">
        <title>O'Prime
        </title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/vendors/flaticon/flaticon.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/vendors/linearicons/style.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/vendors/stroke-icon/style.css">
        <!-- Rev slider css -->
        <link href="<?= site_url()?>/public/site2/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?= site_url()?>/public/site2/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?= site_url()?>/public/site2/vendors/revolution/css/navigation.css" rel="stylesheet">
        <!-- Extra Plugin CSS -->
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/vendors/nice-selector/css/nice-select.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/vendors/owl-carousel/assets/owl.carousel.min.css">
        <link href="<?= site_url()?>/public/site2/vendors/popup/magnific-popup.css" rel="stylesheet">
        <link href="<?= site_url()?>/public/site2/vendors/nice-selector/css/nice-select.css" rel="stylesheet">
        
        <!-- main css -->
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/css/style.css">
        <link rel="stylesheet" href="<?= site_url()?>/public/site2/css/responsive.css">
    </head>
    <body>
        
        <div class="sidebar_menu">
        	<div class="close_icon">
        		<span>X</span>
        	</div>
        	<aside class="left_widget m_img_wd">
        		<img class="img-fluid" src="<?= site_url()?>/public/site2/img/menu-img.jpg" alt="">
        	</aside>
        	<aside class="left_widget insight_widget">
				<div class="f_title">
					<h3>Insights</h3>
					<span></span>
				</div>
				<div class="insight_inner">
					<div class="insight_item">
						<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
						<a href="#">October 25, 2018</a>
					</div>
					<div class="insight_item">
						<p>Efficiently unleash fora cash cross-media information without cross-media.</p>
						<a href="#">October 25, 2018</a>
					</div>
				</div>
			</aside>
       		<aside class="left_widget button_widget">
				<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-1.png" alt="">our brochure</a>
				<a href="#"><img src="<?= site_url()?>/public/site2/img/icon/doc-2.png" alt="">Report 2017</a>
			</aside>
        </div>
        
        <!--================Header Area =================-->
        <header class="header_area">
        	<div class="header_top">
        		<div class="container">
        			<div class="justify-content-between d-flex">
        				<div class="left_side">
							<h6><i class="fa fa-phone"></i>Need any help? Talk to expert : <a href="tel:18004567890">1800 456 7890</a></h6>
						</div>
						<div class="right_side">
							<ul class="nav">
							                            

								<li><a href="#">media center</a></li>
								<li><a href="#">Contact us</a></li>
								<li><a href="#">Career</a></li>
								<li>
									<select class="lan">
										<option value="1">Some option</option>
										<option value="2">Another option</option>
										<option value="3">Potato</option>
									</select>
								</li>
							</ul>
						</div>
        			</div>
        		</div>
        	</div>
        	<div class="header_menu">
        		<div class="container">
					<nav class="navbar navbar-expand-lg navbar-light bg-light">
						<a class="navbar-brand" href="index.html"><img src="<?= site_url()?>/public/site2/img/logo.png" height="55px" width="153px" alt="" ></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span> 
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="nav navbar-nav ml-auto">
								<li class="dropdown submenu active">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=lang('Home')?></a>
									
								</li>

								<li class="dropdown submenu">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= lang('About Us')?></a>
									<ul class="dropdown-menu">
										<li><a href="<?= site_url('about')?>"><?=lang('About Us')?></a></li>
										<li><a href="<?= site_url('about-story')?>"><?=lang('Our Story')?></a></li>
										<li><a href="<?= site_url('about-office')?>"><?=lang('Our offices')?></a></li>
										<li><a href="<?= site_url('about-mission')?>"><?=lang('Mission, Values & Objectives')?></a></li>
										<li><a href="<?= site_url('about-partners')?>"><?=lang('Partners')?></a></li>
										<li><a href="<?= site_url('about-choose')?>"><?=lang('Why Choose Us')?></a></li>
										<li><a href="<?= site_url('about-testimonial')?>"><?=lang('Testimonials')?></a></li>
									</ul>
								</li>
								<li class="dropdown submenu mega_menu tab-demo">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= lang('Services')?> </a>
									<ul class="dropdown-menu">
										<li>
											<div class="row">
												<div class="col-lg-4 tabHeader">
													<ul class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
														<li class="active">
															<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><?= lang('Data & Business Intelligence')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><?= lang('Systems Engineering')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><?= lang('Systems Security')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Cloud Computing')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('ERP & Systems Integration')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Corporate Training & Licensing')?></a>
														</li>
													</ul>
												</div>
												<div class="col-lg-8">
													<div class="tab-content tabContent" id="v-pills-tabContent">
														<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
															<ul class="nav flex-column">
																	<li><a href="<?= site_url('services/intelligence-database')?>"><?= lang('Database Administration')?> </a></li>
																	<li><a href="<?= site_url('services/intelligence-modelling')?>"><?= lang('Data Modelling')?></a></li>
																	<li><a href="<?= site_url('services/intelligence-intelligence')?>"><?= lang('Business Intelligence')?></a></li>
																	
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('services/engineering-modelling')?>"><?= lang('Systems Modelling')?>  </a></li>
																<li><a href="<?= site_url('services/engineering-development')?>"><?= lang('Development and Automation')?></a></li>
																<li><a href="<?= site_url('services/engineering-integration')?>"><?= lang('Integration and Customizations')?></a></li>
																
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('services/security-web-application')?>"><?= lang('Web Application Security')?> </a></li>
																<li><a href="<?= site_url('services/security-risk')?>"><?= lang('Security Assessment & Risk Mitigation')?></a></li>
																<li><a href="<?= site_url('services/security-policies')?>"><?= lang('Policies, Incidence Response and Training')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('services/cloud-computing-infrasture')?>"><?= lang('Cloud Infrastructure')?></a></li>
																<li><a href="<?= site_url('services/cloud-computing-google')?>"><?= lang('Google Cloud')?> </a></li>
																<li><a href="<?= site_url('services/cloud-computing-amazon')?>"><?= lang('Amazon Web Services')?></a></li>
																<li><a href="<?= site_url('services/cloud-computing-azur')?>"><?= lang('Microsoft Azure')?></a></li>
																<li><a href="<?= site_url('services/cloud-computing-digital')?>"><?= lang('Digital Ocean')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('services/erp-business')?>"><?= lang('Business Process Automation')?></a></li>
																<li><a href="<?= site_url('services/erp-erp')?>"><?= lang('ERP Consulting ')?></a></li>
																<li><a href="<?= site_url('services/erp-crm')?>"><?= lang('CRM Consulting')?></a></li>
																<li><a href="<?= site_url('services/erp-hrm')?>"><?= lang('HRM Consulting')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('services/corporate-training-microsoft')?>"><?= lang('Microsoft Products & Training')?></a></li>
																<li><a href="<?= site_url('services/corporate-training-helpdesk')?>"><?= lang('HelpDesk Products & Training ')?></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
								
								<li class="dropdown submenu mega_menu tab-demo">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= lang('Solutions')?> </a>
									<ul class="dropdown-menu">
										<li>
											<div class="row">
												<div class="col-lg-4 tabHeader">
													<ul class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
														<li class="active">
															<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><?= lang('Business Management')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><?= lang('Hospitality & Leisure')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><?= lang('Education')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Social')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Finance')?></a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Events')?> </a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('Transport & Logistice')?> </a>
														</li>
														<li>
															<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><?= lang('General')?></a>
														</li>
													</ul>
												</div>
												<div class="col-lg-8">
													<div class="tab-content tabContent" id="v-pills-tabContent">
														<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
															<ul class="nav flex-column">
																	<li><a href="<?= site_url('solutions/business-primeone')?>"><?= lang('PrimeOne')?> </a></li>
																	<li><a href="<?= site_url('solutions/business-primesms')?>"><?= lang('PrimeSMS')?></a></li>
																	<li><a href="<?= site_url('solutions/business-primepos')?>"><?= lang('PrimePOS')?></a></li>
																	
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/hospitality-primehotel')?>"><?= lang('PrimeHotels')?> </a></li>
																
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/education-primecampus')?>"> <?= lang('PrimeCampus')?></a></li>
																<li><a href="<?= site_url('solutions/education-certverify')?>"><?= lang('Certverify')?></a></li>
																<li><a href="<?= site_url('solutions/education-alumnia')?>"><?= lang('Alumnia')?></a></li>
																<li><a href="<?= site_url('solutions/education-iapply')?>"><?= lang('IApply')?></a></li>
																<li><a href="<?= site_url('solutions/education-apson')?>"><?= lang('APSON')?></a></li>
																<li><a href="<?= site_url('solutions/education-revise')?>"><?= lang('Revise')?></a></li>


															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/social-apson')?>"><?= lang('APSON')?></a></li>
																<li><a href="<?= site_url('solutions/social-primecx')?>"><?= lang('PrimeCX')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/finance-primepay')?>"><?= lang('PrimePay')?></a></li>
																<li><a href="<?= site_url('solutions/finance-primefinx')?>"><?= lang('PrimeFinx')?></a></li>
																<li><a href="<?= site_url('solutions/finance-mifos-x')?>"><?= lang('Mifos X')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/events-eventa')?>"><?= lang('Events')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/transport-primetlms')?>"><?= lang('PrimeTLMS')?></a></li>
															</ul>
														</div>
														<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
															<ul class="nav flex-column">
																<li><a href="<?= site_url('solutions/general-primecms')?>"><?= lang('PrimeCMS')?></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>

								<li class="dropdown submenu">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= lang('Industries')?></a>
									<ul class="dropdown-menu">
										<li><a href="<?= site_url('industries-education')?>"><?= lang('Education')?></a></li>
										<li><a href="<?= site_url('industries-transport')?>"><?= lang('Transports & Logistics')?></a></li>
										<li><a href="<?= site_url('industries-hospitality')?>"><?= lang('Hospitality & Leisure')?></a></li>
										<li><a href="<?= site_url('industries-banking')?>"><?= lang('Banking & Finance')?></a></li>
										<li><a href="<?= site_url('indutries-marketing')?>"><?= lang('Marketing Automation')?></a></li>
										<li><a href="<?= site_url('indusstries-service-businesses')?>"><?= lang('Service Businesses')?></a></li>
										<li><a href="<?= site_url('industries-health')?>"><?= lang('Health')?></a></li>

									</ul>
								</li>
								<li class="dropdown submenu">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= lang('Blog')?></a>
									<ul class="dropdown-menu right">
										<li><a href="blog-grid.html"><?= lang('Blog Grid')?></a></li>
										<li><a href="blog-list.html"><?= lang('Blog Masonry')?></a></li>
										<li><a href="blog-left-sidebar.html"><?= lang('Blog with leftside bar')?></a></li>
										<li><a href="blog-right-sidebar.html"><?= lang('Blog with rightside bar')?></a></li>
										<li><a href="single-blog.html"><?= lang('Blog details')?></a></li>
									</ul>
								</li>
								
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a class="popup-with-zoom-anim" href="#test-search"><i class="icon icon-Search"></i></a></li>
								<li class="side_menu"><a href="#"><img src="<?= site_url()?>/public/site2/img/icon/menu.png" alt=""></a></li>
							</ul>
						</div>
					</nav>
				</div>
        	</div>
        </header>
        <!--================End Header Area =================-->
        
       